% Notes for a talk about derived Morita theory in a seminar on derived
% categories.
% Written by Aras Ergus.

% This work is licensed under a Creative Commons Attribution-ShareAlike 4.0
% International License.
% See the following website for details:
% https://creativecommons.org/licenses/by-sa/4.0/

\documentclass[12pt,a4paper]{scrartcl}
\usepackage{amsfonts}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm, thmtools}
\usepackage[english]{babel}
\usepackage{color}
\usepackage{dsfont}
\usepackage{emptypage}
\usepackage{enumerate}
\usepackage{faktor}
\usepackage[T1]{fontenc}
\usepackage{ifthen}
\usepackage{imakeidx}
\usepackage{indentfirst}
\usepackage[utf8]{inputenc}
\usepackage{lmodern}
\usepackage{nameref, hyperref, cleveref}
\usepackage[cal=boondoxo]{mathalfa}
\usepackage{mathabx}
\usepackage{mathtools}
\usepackage{tikz}
\usepackage{tikz-cd}
\usepackage{xspace}
\usepackage{yfonts}
\usepackage[
  type={CC},
  modifier={by-sa},
  version={4.0},
]{doclicense}

\usepackage{stuff}

\bibliographystyle{alpha}

\title{Morita Theory \\ for Derived Categories of Algebras}
\author{Aras Ergus}
\date{February 6, 2016}

\newcommand{\ho}{\mathcal{K}}
\newcommand{\der}{\mathcal{D}}
\newcommand{\per}{\operatorname{per}}

\begin{document}
\maketitle
\doclicenseThis

\tableofcontents

\vspace{2em}

These notes and the talk they are typed for is essentially an exposition of
(parts of) \cite{Keller98}. Some examples and proof ideas are taken
from \cite{Schwede04}.

The notations and conventions which were introduced in previous talks were
tried to be preserved as long as it made sense to do so. Furthermore, the
following conventions are running assumptions for the whole text.
\begin{itemize}
\item $k$ is a commutative ring and all maps, functors \etc are assumed
to be $k$-linear.
\item Modules are \emph{right} modules.
\item When we consider complexes, we use the cohomological notation, \ie
the differentials \emph{increase} the degree.
\item When we talk of a full subcategory, it is implicit that the
subcategory is closed under isomorphisms in the ambient category. (This
property was sometimes called ``repleteness'' or ``strictness''.)
\end{itemize}

\clearpage

\section{Differential Graded Algebras}

\begin{definition}
A \emph{a differental graded $k$-algebra} (or \emph{DG algebra}) is a
$\ZZ$-graded $k$-algebra $A = \bigoplus_{p \in \ZZ} A^p$ equipped with a
$k$-linear differential $d \colon A \to A$ which is homogeneous of degree $1$
and satisfies the graded Leibniz rule, \ie
\begin{itemize}
\item $A^p \cdot A^q \subseteq A^{p + q}$ (``graded''),
\item $d(A^p) \subseteq A^{p + 1}$ (``homogeneous of degree $1$''),
\item $d ^ 2 = 0$ (``differential''),
\item $d(a \cdot b) = (da) \cdot b + (-1)^p \cdot a \cdot (db)$ for all
$a \in A^p$, $b \in A$ (``Leibniz rule'').
\end{itemize}

A \emph{morphism of DG algebras} is a morphism of graded algebras which
commutes with the differentials.
\end{definition}

\begin{example} \label{ordinary-to-dg}
Every $k$-algebra $R$ (in the usual sense) yields a DG algebra
$A = \bigoplus_{p \in \ZZ} A^p$ by setting
\[
A^p =
\begin{cases}
R & p = 0 \\
0 & p \neq 0
\end{cases},
\]
$d = 0$ and extending the multiplication of $R$ by zero.
\end{example}

\begin{notation}
Given an ordinary algebra $R$, we will sometimes abuse notation and denote
also the DG algebra obtained from it by $R$.
\end{notation}

\begin{remark}
In the following we are going to generalize some constructions for algebras to
DG algebras and use the same notation for both of types of constructions. This
abuse of notation is justified by the fact that if one applies the
constructions in the ``DG world'' to the DG algebra obtained from an ordinary
algebra as in \autoref{ordinary-to-dg}, one gets the corresponding
constructions for complexes over that algebra.
\end{remark}

We now fix DG algebras $A$ and $B$ until the end of this section.

\begin{definition}
A \emph{(right) a differential graded module over $A$} (or
\emph{DG $A$-module}) is a $\ZZ$-graded (right) $A$-module
$M = \bigoplus_{p \in \ZZ} M^p$ endowed with a $k$-linear differential
$d \colon M \to M$ which is homogeneous of degree $1$ and satisfies the
graded Leibniz rule, \ie
\begin{itemize}
\item $M^p \cdot A^q \subseteq M^{p + q}$ (``graded''),
\item $d(M^p) \subseteq M^{p + 1}$ (``homogeneous of degree $1$''),
\item $d^2 = 0$ (``differential''),
\item $d(m \cdot a) = (dm) \cdot a + (-1)^p \cdot m \cdot (da)$ for all
$m \in M^p$, $a \in A$ (``Leibniz rule'').
\end{itemize}
\end{definition}

\emph{Left} DG modules are defined analogously.

\begin{definition}
A \emph{morphism of DG modules} is a morphism of underlying graded modules
which is homogeneous of degree $0$ and commutes with the differential.

This concept of morphisms yields the category $\text{DG-Mod}-A$ of DG
$A$-modules.
\end{definition}

\begin{definition}
The \emph{opposite} $A^{\operatorname{op}}$ of a
DG algebra is defined to be the same graded $k$-module with the same
differential, equipped with the multiplication
\[
a \cdot_{A^{\operatorname{op}}} a' = (-1)^{p \cdot p'} a' \cdot_A a
\]
for $a \in (A^{\operatorname{op}})^p$,
$a' \in (A^{\operatorname{op}})^{p'}$.
\end{definition}

\begin{remark}
A left DG $A$-module is ``the same'' as a (right)
DG $A^{\operatorname{op}}$-module.
\end{remark}

\begin{example}
In the situation of \autoref{ordinary-to-dg}, the category of DG modules over
(the DG algebra obtained from) an ordinary algebra can be identified with
the category of complexes over that algebra (by ``forgetting all the graded
pieces of the algebra except the $0$-th piece``).
\end{example}

\begin{example}
Every DG algebra becomes a DG module over itself via the multiplication action.
\end{example}

\begin{remark} \label{hom-as-cycles}
For all DG $A$-modules $M$, the map
\[
\Hom[\text{DG-Mod}-A]{A}{M} \to Z^0 M, \ f \mapsto f(1)
\]
is an isomorphism.
\end{remark}

\begin{definition}
A \emph{DG $A$-$B$-bimodule} $X = \bigoplus_{p \in \ZZ} X^p$ is
simultaneously graded left $A$-module and a graded graded right $B$-module
\st the two actions agree on $k$, commute and $X$ is endowed with a $k$-linear
homogeneous differential of degree $1$ which satisfies
\[
d(a \cdot x \cdot b) =
(da) \cdot x \cdot b +
(-1)^p \cdot a \cdot (dx) \cdot b +
(-1)^{p + q} \cdot a \cdot x \cdot (db)
\]
for all $a \in A^p$, $x \in X^q$ and $b \in B$.
\end{definition}

\begin{definition}
We define the DG algebra $A \otimes_k B$ to be
\[
(A \otimes_k B)^n = \otimes_{p + q = n} A^p \otimes_k B^q,
\]
equipped with the differential given by
\[
d(a \otimes b) = (da) \otimes b + (-1)^p a \otimes (db)
\]
for $a \in A^p$, $b \in B$, and the multiplication given by
\[
(a \otimes a') \cdot (b \otimes b') =
(-1)^{q \cdot p'} (a \cdot a') \otimes (b \cdot b')
\]
for $a \in A$, $a' \in A^{p'}$, $b \in B^q$, $b' \in B$.
\end{definition}

\begin{remark}
A DG $A$-$B$-bimodule is ``the same'' as a DG
$(A^{\operatorname{op}} \otimes B)$-module.
\end{remark}

\begin{definition}
Given a DG $A$-module $M$ and a DG $A$-$B$-bimodule $X$, we define their
\emph{tensor product} as
\[
M \otimes_A X = \faktor{\left( \bigoplus_{m \in \ZZ}
\left( \bigoplus_{p + q = m} M^p \otimes_k X^q \right) \right)}
{\braket{ma \otimes x - m \otimes ax \mid a \in A, m \in M, x \in X}},
\]
equipped with the differential induced by the rule
\[
d(m \otimes x) = (dm) \otimes x + (-1)^p \cdot m \otimes (dx)
\]
for all $m \in M^p$ and $x \in X$.
\end{definition}

\begin{remark}
Let $X$ be a DG $A$-$B$ bimodule and $M$ a DG $A$-module.
\begin{itemize}
\item $M \otimes_A X$ can be endowed with the structure of a DG $B$-module
via the action inherited from $X$.
\item The assignment $M \mapsto M \otimes_A X$ is functorial.
\end{itemize}
\end{remark}

\begin{definition} \label{hom-bimodule}
Given a DG $A$-$B$-bimodule $X$ and a DG $B$-module $N$, we define
$\CHom[B]{X}{N}$ by setting
% May the gods of typography forgive my sins...
\begin{align*}
(\CHom[B]{X}{N})^n & =
\{f \colon X \to N \mid
\begin{aligned}[t]
& f \text{ graded } B\text{-module homomorphism,} \\
& \text{homogeneous of degree } n \}
\end{aligned}
\end{align*}
with differential given by
\[
df = d \circ f - (-1)^n f \circ d
\]
for all $f \in \CHom[B]{X}{N})^n$.
\end{definition}

\begin{remark}
Let $N$ be a DG $B$-module and $X$ be a DG $A$-$B$ bimodule.
\begin{itemize}
\item $\CHom[B]{X}{N}$ can be endowed with the structure of a DG
$A$-module via $(f \cdot a)(x) = f (a \cdot x)$ for all
$f \in (\CHom[B]{X}{N})^n$ and $x \in X$.
\item The assignment $N \mapsto \CHom[B]{X}{N}$ is functorial.
\end{itemize}
\end{remark}

\begin{remark}
Given a DG $A$-$B$-bimodule $X$, we obtain an adjunction
\[
(\bullet) \otimes_A X  \colon \text{DG-Mod}-A \rightleftarrows
\text{DG-Mod}-B \colon \CHom[B]{X}{\star}.
\]
\end{remark}

\begin{remark}
Let $N$ and $N'$ be DG $B$ modules.
\begin{itemize}
\item Setting $A$ to be $k$ (hence considering $N$ as a $k$-$B$-module) in
\autoref{hom-bimodule}, we obtain a DG $k$-module $\CHom[B]{N}{N'}$.
\item  $Z^0(\CHom[B]{N}{N'})$ consists of morphisms of $DG$ $B$-modules from
$N$ to $N'$ (cf \autoref{hom-as-cycles}).
\item $\CEnd[B]{N} = \CHom[B]{N}{N}$ has the structure of a DG algebra whose
multiplication is given by composition of morphisms. (Note that if
$f \colon N \to N$ is of degree $i$ and $g \colon N \to N$ is of degree $j$,
then $f \circ g \colon N \to N$ is of degree $i + j$, \ie composition
indeed respects the degree.)
\item $\CHom[B]{N}{N'}$ has the structure of a DG $\CEnd[B]{N}$-module, where
(homogeneous pieces of) $\CEnd[B]{N}$ acts on (homogeneous pieces of)
$\CHom[B]{N}{N'}$ via precomposition.
\end{itemize}
\end{remark}


\section{Recollections on Derived Categories}

Next, we want to define homotopy categories and the derived categories in the
DG setting, which generalize the constructions we had in previous talks.

$A$ and $B$ will again be fixed DG algebras in this section.

\begin{definition}
A morphism $f \colon M \to N$ of DG $A$-modules is called \emph{null homotopic}
if there exists a morphism $r \colon M \to N$ of the underlying graded
$A$-modules which is homogenous of degree $-1$ \st $f = d_N r + r d_M$ holds.
In that case, one also writes $f \simeq 0$.
\end{definition}

\begin{definition}
The \emph{homotopy category} $\ho A$ is given by:
\begin{itemize}
\item $\ob(\ho A)$ is the class of DG $A$-modules,
\item $\Hom[\ho A]{M}{N} =
\faktor{\Hom[\text{DG-Mod}-A]{M}{N}}
{\class{f \in \Hom[\text{DG-Mod}-A]{M}{N}}{f \simeq 0}}$ for all DG
$A$-modules $M$ and $N$.
\end{itemize}
\end{definition}

\begin{remark}
For a DG $A$-module $M$ and a morphism $f \colon A \to M$ of DG modules,
$f \simeq 0$ iff $f(1) \in B^0M$, so the map
\[
\Hom[\ho A]{A}{M} \to H^0 M, \ \overline{f} \mapsto \overline{f(1)}
\]
is an isomorphism.
\end{remark}

\begin{remark}
In fact, for all DG $A$-modules $M$ and $N$, $B^0(\CHom[A]{M}{N})$ consists of
morphisms $f \colon M \to N$ of DG modules which are null homotopic, hence
we have an isomorphism
\[
\Hom[\ho A]{M}{N} \cong H^0(\CHom[A]{M}{N}).
\]
\end{remark}

We now want to equip $\ho A$ with the structure of a triangulated category
by extending the required constructions from chain complexes to DG modules.

\begin{definition}
The \emph{suspension functor} (or \emph{shift functor}) $(\bullet)[1]$ on
$\text{DG-Mod}-A$ is defined by setting
\begin{itemize}
\item $M[1]^p = M^{p+1}$,
\item $d_{M[1]} = -d_M$,
\item $m \cdot_{M[1]} a = m \cdot_M a$
\end{itemize}
for a DG $A$-module $M$, $m \in M$ and $a \in A$.

The suspension functor descends to $\ho A$ and is denoted likewise as a
functor on $\ho A$.
\end{definition}

\begin{definition}
For a morphism $f \colon M \to N$ of DG $A$-modules, its \emph{(mapping) cone}
$Cf$ is defined by setting
\begin{itemize}
\item $Cf = M[1] \oplus N$ as graded modules,
\item $d_{Cf} = \left(
\begin{matrix}
d_{M[1]} & 0 \\
f        & d_M
\end{matrix} \right)$.
\end{itemize}

Note that $Cf$ comes equipped with a canonical injection $i_f \colon N \to Cf$
and a canonical projection $p_f \colon Cf \to M[1]$ (which are morphisms of
DG $A$-algebras).
\end{definition}

\begin{theorem}
$\ho A$ has a structure of a triangulated category \st
\begin{itemize}
\item The automorphism $\Sigma$ is given by $(\bullet)[1]$.
\item (Distinguished) triangles are given by triangles isomorphic to
\[
M \xrightarrow{\overline{f}} N \xrightarrow{\overline{i_f}}
Cf \xrightarrow{\overline{-p_f}} M[1].
\]
\end{itemize}
\end{theorem}

\begin{proof}[Proof Idea]
One endows $\text{DG-Mod}-A$ with the structure of an exact category given
by the sequences which split as sequences of graded $A$-modules and checks
that this yields a Frobenius category whose stable category is equivalent
to $\ho A$ (in a manner that ``preserves shifts and triangles'').
\end{proof}

\begin{example}
In the setting of \autoref{ordinary-to-dg}, the homotopy category of the DG
algebra associated to an ordinary algebra coincides with the homotopy category
of the ordinary algebra.
\end{example}

\begin{definition}
A morphism $f \colon M \to N$ of DG $A$-modules is called a
\emph{quasi-isomorphism} if the induces morphism $H^*f \colon H^*M \to H^*N$
on homology is an isomorphism.

Let $\mathcal{S}$ denote the class of quasi-isomorphisms.
\end{definition}

\begin{definition}
The \emph{derived category} $\der A$ of $A$ is the localization
$(\ho A)[\mathcal{S}^{-1}]$.
\end{definition}

\begin{remark}
$\der A$ has a unique structure of a triangulated category such that the
localization functor $Q \colon \ho A \to \der A$ is triangulated.
\end{remark}

As with usual algebras, replacing DG modules with ``ones that have better
homotopical behavior'' comes in handy while working with (or even defining)
derived categories.

\begin{definition}
A DG module $M$ is called \emph{acyclic} if $H^iM \cong 0$ for all
$i \in \ZZ$.
\end{definition}

\begin{definition}
A DG module $K$ is called \emph{homotopically projective} (\resp
\emph{homotopically injective} if $\Hom[\ho A]{K}{N} \cong 0$ (\resp
$\Hom[\ho A]{N}{K} \cong 0$) for all acyclic DG modules $N$.
\end{definition}

\begin{example}
$A$ is a homotopically projective DG $A$-module.
\end{example}

\begin{definition}
Let $\ho_p A$ (\resp $\ho_i$) be the full subcategory of homotopically
projective (\resp homotopically injective) objects in $\ho A$.
\end{definition}

Now we will state, without proving, some important facts about homotopy
categories and derived categories (of DG algebras). Some previous talks dealt
with analogous statements in the case of ordinary algebras, and the proofs in
the DG setting are also analogous.

Let $X$ be a DG $A$-$B$-bimodule for the rest of this section.

\begin{proposition} \label{h-proj-res}
There is an ``h-projective resolution'' functor
$p \colon \ho A \to \ho_p A$ such that for every DG $A$-module $M$,
there is a triangle (which is, in $\ho A$, unique up to isomorphisms extending
$\id_M$)
\[
pM \to M \to N \leadsto,
\]
where $N$ is acyclic.
\end{proposition}

\begin{remark}
There is a ``dual version'' of \autoref{h-proj-res} for homotopically
injective DG algebras, \ie there is an ``h-injective resolution'' functor
$i \colon \ho A \to \ho_i A$.
\end{remark}

\begin{remark} Here are some facts about $p$ \resp $i$.
\begin{itemize}
\item Since $p$ (\resp $i$) vanishes on acyclic complexes, it descends to a
functor $\der A \to \ho_p A$ (\resp  $\der A \to \ho_i A$).
\item The localization functor $Q \colon \ho A \to \der A$ restricts to an
equivalence $\ho_p A \to \der A$ (\resp $\ho_i A \to \der A$) with left
adjoint quasi-inverse $p$ (\resp right adjoint quasi-inverse $i$).
\item In particular, one obtains natural isomorphisms
\[
\Hom[\der A]{\bullet}{Q(\star)} \xrightarrow{\cong}
\Hom[\ho A]{p(\bullet)}{\star}
\]
\resp
\[
\Hom[\ho A]{\bullet}{i(\star)} \xrightarrow{\cong}
\Hom[\der A]{Q(\bullet)}{\star}.
\]
\end{itemize}
\end{remark}

\begin{corollary}
For every DG $A$-module $M$ we have isomorphisms
\[
\Hom[\der A]{A}{M} \xrightarrow{\cong} \Hom[\ho A]{pA}{M}
\xrightarrow{\cong} \Hom[\ho A]{A}{M} \xrightarrow{\cong} H^0M.
\]
\end{corollary}

\begin{definition}
Given a functor $F \colon \ho A \to \mathcal{C}$, its \emph{total left derived
functor} $\mathbb{L}F$ is defined as
$F \circ p \colon \der A \to \mathcal{C}$. Similarly, the \emph{total right
derived functor} $\mathbb{R}F$ of $F$ is defined as $F \circ i$.
\end{definition}

\begin{notation}
We also write $(\bullet) \otimes_A^{\mathbb{L}} X$
instead of $\mathbb{L}((\bullet) \otimes_A X)$.
\end{notation}

\begin{remark}
The functors $\mathbb{L}((\bullet) \otimes_A X)$ and
$\mathbb{R}\CHom[B]{X}{\star}$ are triangulated.
\end{remark}

\begin{remark}
There is an adjunction
\[
(\bullet) \otimes_A^{\mathbb{L}} X \colon \der A \rightleftarrows
\der B \colon \mathbb{R}\CHom[B]{X}{\star}.
\]
\end{remark}

\begin{remark}
Given DG $A$-modules $M$ and $N$, we have an isomorphism
\[
H^n(\mathbb{R}\CHom[A]{M}{N}) \cong \Hom[\der A]{M}{N[n]}.
\]
\end{remark}

Now let us recall perfect complexes.

\begin{definition}
The subcategory $\per A$ of \emph{perfect DG $A$-modules} is the smallest
full triangulated subcategory of $\der A$ containing $A$ and closed under
direct summands.
\end{definition}

\begin{remark}
In the setting of \autoref{ordinary-to-dg}, perfect DG modules over the
DG algebra associated to an ordinary algebra coincide with perfect complexes
over that algebra.
\end{remark}

\begin{remark} \label{per-iff-sum-preserving}
A DG $A$-module $K$ is in $\per A$ iff the functor $\Hom[\der A]{K}{\bullet}$
preserves infinite direct sums.
\end{remark}

The following principle is very helpful while showing that certain
triangulated functors are equivalences.

\begin{proposition}[infinite d\'evissage] \label{devissage}
A full triangulated subcategory of $\der A$ is equal to $\der A$ iff it
contains $A$ (seen as a DG module over itself) and is closed under infinite
direct sums.
\end{proposition}

\section{Derived Equivalences}

$A$ and $B$ continue to be fixed DG algebras in this section. Furthermore,
let $X$ be a DG $A$-$B$-bimodule throughout the section.

We start with a criterion for deciding when derived tensor products induce
derived equivalences.

\begin{proposition} \label{std-eq}
The following are equivalent:
\begin{enumerate}[i)]
\item $(\bullet) \otimes_A^\mathbb{L} X \colon \der A \to \der B$ is an
equivalence.
\item $(\bullet) \otimes_A^\mathbb{L} X$ induces an equivalence
$\per A \to \per B$.
\item The object $T \coloneqq A \otimes_A^\mathbb{L} X$ satisfies the
following:
\begin{enumerate}[a)]
\item The map
\[
H^nA \cong \Hom[\der A]{A}{A[n]} \to \Hom[\der B]{T}{T[n]}
\]
is an isomorphism for all $n \in \ZZ$.
\item $T$ is in $\per B$.
\item The smallest full triangulated subcategory of $\der B$ containing $T$
and closed under forming direct summands is equal to $\per B$.
\end{enumerate}
\end{enumerate}
\end{proposition}

Such equivalences of derived categories are often called
\emph{standard derived equivalences}.

\begin{proof}
``$(i) \Rightarrow (ii)$'': By the intrinsic characterization of $\per B$
(\autoref{per-iff-sum-preserving}), every derived equivalence restricts to
an equivalence of subcategories of perfect DG modules.

``$(ii) \Rightarrow (iii)$'': We see that
\begin{enumerate}[a)]
\item follows by the fact that
$(\bullet) \otimes_A^\mathbb{L} X$ fully faithful (on $\der A$),
\item by the fact that it takes perfect complexes to perfect complexes,
\item by the fact that $\per B$ is the essential image of $\per A$
under $(\bullet) \otimes_A^\mathbb{L} X$.
\end{enumerate}

``$(iii) \Rightarrow (i)$'': We start by showing that
$(\bullet) \otimes_A^\mathbb{L} X$ is fully faithful. For this it is enough
to show that the adjunction unit
\[
\varphi_M \colon M \to \mathbb{R}\CHom[B]{X}{M \otimes_A^\mathbb{L} X}
\]
is an isomorphism in $\der A$ for all DG $A$-modules $M$.

Note that
$T = A \otimes_A^\mathbb{L} X = pA \otimes_A X = A \otimes_A X$ is
isomorphic to $X$ seen as a DG $B$-module. This means in particular
that the functor $\mathbb{R}\CHom[B]{X}{\star}$ is not only triangulated, but
also preserves direct sums since $T$ and hence $X$ is perfect as a DG
$B$-module. Further, also $(\bullet) \otimes_A^\mathbb{L} X$ is
triangulated and preserves direct sums. Hence we can use infinite d\'evissage
(\autoref{devissage}) on $\der A$ to reduce the claim to the statement that 
\[
\varphi_A \colon A \to \mathbb{R}\CHom[B]{X}{A \otimes_A^\mathbb{L} X} =
\mathbb{R}\CHom[B]{X}{T} \cong \mathbb{R}\CHom[B]{T}{T}
\]
is an isomorphism.

Under this identification we see that
\[
H^n \varphi_A \colon H^n A \to H^n (\mathbb{R}\CHom[B]{X}{T}) \cong
H^n (\mathbb{R}\CHom[B]{T}{T}) 
\]
is the isomorphism in $(a)$, so $\varphi_M$ is an isomorphism in $\der A$
since it is a quasi-isomorphism on the level of DG modules.

In order to see that $(\bullet) \otimes_A^\mathbb{L} X$ is essentially
surjective, we observe that its essential image is closed under direct
summands since it is fully faithful and direct summands correspond to
idempotent morphisms. Combining this with the fact that
$(\bullet) \otimes_A^\mathbb{L} X$ is triangulated, $(c)$ yields that
$\per B$, hence $B$ is in its essential image. Now infinite d\'evissage
implies that the essential image of $(\bullet) \otimes_A^\mathbb{L} X$ is
whole $\der B$ since it preserves infinite direct sums.
\end{proof}

\begin{definition}
A DG $A$-module $M$ is called a \emph{compact generator} of $\der A$ if
it is in $\per A$ as an object of $\der A$ and the smallest full
triangualated subcategory of $\der A$ containing $M$ and
closed under forming direct summands is equal to $\per A$.
\end{definition}

\begin{corollary} \label{quasi-to-der}
Let $f \colon A \to B$ be a morphism of DG algebras. Then $f$ endows $B$ with
the structure of a DG $A$-module, so $B$ becomes a DG $A$-$B$-bimodule.

Now assume that $f$ is a quasi-isomorphism, \ie $H^* f \colon H^* A \to H^* B$
is an isomorphism. Then $A \otimes_A^\mathbb{L} B \cong B$ satisfies all the
conditions in the third part of \autoref{std-eq}, so
$(\bullet) \otimes_A^\mathbb{L} B \colon \der A \to \der B$ is a derived
equivalence.
\end{corollary}

\begin{corollary} \label{eq-to-der-end}
Let $M$ be a homotopically projective compact generator of $\der A$. Then
$\CEnd[A]{M} \otimes_{\CEnd[A]{M}}^\mathbb{L} M \cong M$ satisfies all
the conditions in the third part of \autoref{std-eq}, where the condition
$(a)$ is given by the isomorphism
\[
H^n(\CEnd[A]{M}) \to \Hom[\der A]{M}{M[n]} \cong \Hom[\ho A]{M}{M[n]}
\cong H^n(\CEnd[A]{M}).
\]
Hence $(\bullet) \otimes_{\CEnd[A]{M}}^\mathbb{L} M \colon
\der (\CEnd[A]{M}) \to \der A$ is a derived equivalence.
\end{corollary}

We need an auxiliary construction before we can move on.

\begin{definition}
For a DG algebra $A$ let the DG subalgebra $A_-$ of $A$ be given by
\[
(A_-)^p =
\begin{cases}
Z^0 A & p = 0 \\
A^p   & p < 0 \\
0     & p > 0
\end{cases}.
\]
\end{definition}

Now we can prove some classical theorems about derived equivalences.

\begin{theorem} \label{eq-cond-for-der}
For (ordinary) algebras $R$ and $S$ the following are equivalent:
\begin{enumerate}[i)]
\item There is a triangulated equivalence $\der R \to \der S$.
\item There is a triangulated equivalence $\per R \to \per S$.
\item There is a compact generator $T$ of $\der S$ \st
$\Hom[\der S]{T}{T} \cong R$ and $\Hom[\der B]{T}{T[n]} \cong 0$ for
$n \in \ZZ \setminus \setbrack{0}$
\end{enumerate}
\end{theorem}

Such complexes as in part $(iii)$ are sometimes called \emph{tilting
complexes}.

\begin{proof}
``$(i) \Rightarrow (ii)$'': Follows from the intrinsic definition of $\per A$
and $\per B$.

``$(ii) \Rightarrow (iii)$'': Setting $T$ to be the image of $R$ under the
equivalence $\per R \to \per S$ does the job.

``$(iii) \Rightarrow (i)$'': Since $p$ preserves endomorphisms in $\der A$
and being in $\per A$, we can without loss of generality assume that $T$ is
homotopically projective. Then, by \autoref{eq-to-der-end}, we know that there
is a triangulated equivalence
$\der (\CEnd[S]{T}) \xrightarrow{\simeq} \der S$.

Now, since $H^n(\CEnd[S]{T}) \cong \Hom[\der B]{T}{T[n]}$ is concentrated in
degree $0$, the inclusion $(\CEnd[S]{T})_- \to \CEnd[S]{T}$ induces a
isomorphisms on homology, so by \autoref{quasi-to-der} there is a triangulated
equivalence $\der ((\CEnd[S]{T})_-)  \xrightarrow{\simeq} \der(\CEnd[S]{T})$.

Similarly, homology induces a morphism $(\CEnd[S]{T})_{-} \to R$ of DG
algebras which is an isomorphism on homology by the assumption and the fact
that all differentials of $R$ are zero. Invoking \autoref{quasi-to-der} again,
we obtain a triangulated equivalence
$\der (\CEnd[S]{T})_-  \xrightarrow{\simeq} \der(R)$.

In total, we have a chain of triangulated equivalences
\[
\der(R) \xleftarrow{\simeq} \der ((\CEnd[S]{T})_-)  \xrightarrow{\simeq}
\der(\CEnd[S]{T}) \xrightarrow{\simeq} \der S.
\]
\end{proof}

Note that this proof doesn't work for general DG algebras since it involves
truncations and taking homology.

One might ask when triangulated equivalences between derived categories are
given by standard equivalences.

\begin{proposition} \label{der-is-std-if-flat}
Let $R$ be an algebra, $S$ a flat algebra and $F \colon \der R \to \der S$
a triangulated equivalence. Then there exists a complex $Y$ of
$R$-$S$-bimodules \st $(\bullet) \otimes_{R}^\mathbb{L} Y$ is a triangulated
equivalence.
\end{proposition}

\begin{proof}
Following ``$(i) \Rightarrow (iii)$'' in \autoref{eq-cond-for-der}, we see
that $T \coloneqq F R$ is compact generator of $\der S$ \st
$\Hom[\der B]{T}{T} \cong R$ and $\Hom[\der B]{T}{T[n]} \cong 0$ for
$n \in \ZZ \setminus \setbrack{0}$. We can without loss of generality
assume that $T$ is homotopically projective by possibly replacing it with
a homotopically projective resolution.

We let $E \coloneqq (\CEnd[S]{T})_-$ act on $T$ via
restriction along its inclusion to $\CEnd[S]{T}$. Let $T_p$ be a homotopically
projective resolution of $T$ as a DG $(E \otimes_k S)$-module. We set
\[
Y \coloneqq R \otimes_E T_p,
\]
where $E$ acts on $R$ by the (DG) algebra homomorphism
$h \colon E = (\CEnd[S]{T})_- \to R$ induced by taking homology.

Next, we want to show that
\[
h \otimes_E \id_{T_p} \colon
E \otimes_E T_p \to
R \otimes_E T_p
\]
is a quasi-isomorphism of complexes over $S$, so that we have a chain
of quasi-isomorphisms
\[
R \otimes^{\mathbb{L}}_R (R \otimes_E T_p) \sim
R \otimes_R (R \otimes_E T_p) \xrightarrow{\sim}
R \otimes_E T_p \xleftarrow{\sim}
E \otimes_E T_p 
\xrightarrow{\sim} T_p \xrightarrow{\sim} T.
\]

We will do this by showing that its cone is acyclic. Note that the full
subcategory of DG $E$-$S$-bimodules $Z$ for which $(\bullet) \otimes_E Z$
preserves acyclic objects is triangulated and closed under direct sums.
Hence, by inifinite d\'evissage, the statement that
$(\bullet) \otimes_E T_p$ preserves acyclicity can be
reduced to the fact that
\[
(\bullet) \otimes_E
(E^{\mathrm{op}} \otimes_k S) \cong
(\bullet) \otimes_E
(E \otimes_k S)
\]
preserves acyclicity, which follows from the fact that $S$ is flat.

Now, by \autoref{std-eq}, $(\bullet) \otimes_E Y$ is derived equivalence.
\end{proof}

In fact, an analogous statement can be proven assuming that $R$ (but not
necessarily $S$) is flat (\cf Theorem 3.13 in \cite{Schwede04}).

\section{Examples}

In this section we deal with some examples with less complete proofs.

\begin{example} \label{tilting-a-three}
In previous talks we have seen that two orientations of the same Dynkin quiver
have equivalent derived categories of representation categories. We will
now give an explicit example of such a derived equivalence which is induced
by a \emph{tilting module} (aka ``tilting complex which is concentrated
at degree zero'').

We fix a base field $K$ and consider the orientations
\[
Q \coloneqq (1 \rightarrow 2 \rightarrow 3) \text{ and }
Q' \coloneqq (1 \leftarrow 2 \rightarrow 3)
\]
of $A_3$.

Let $T = P_1 \oplus P_2 \oplus S^2$ (where the standard projectives and
standard simples are constructed over $KQ$). First of all, one can compute
that
\[
\End[KQ]{T} \cong
\class{\left(\begin{matrix}
* & * & * \\
0 & * & 0 \\
0 & 0 & *
\end{matrix}\right)}{\ldots} \cong KQ'.
\]

Next we consider the projective resolution
\[
0 \to KQ \xrightarrow{\operatorname{incl} } P_1 \oplus P_2 \oplus P_2 \to S_2
\to 0
\]
of $S_2$.

This sequence means in particular that the smallest full triangulated
subcategory of $\der (KQ)$ that contains $T$ and is closed under direct
summands also contains $KQ$ since it must contain $P_1 \oplus P_2 \oplus P_2$
as a direct summand of $T^2$ and $S_2$ a direct summand of $T$. Moreover, $T$
is quasi-isomorphic to
\[
\ldots \to 0 \to
KQ \xrightarrow{\left(\substack{\operatorname{incl} \\ 0}\right)}
(P_1 \oplus P_2 \oplus P_2) \oplus (P_1 \oplus P_2)
\to 0 \to \ldots,
\]
which is perfect as bounded complex of projectives. Hence $T$ is a small
generator of $\der (KQ)$. Doing some further calculations, one also sees that
$\operatorname{Ext}^n(T,T) = 0$ for $n \neq 0$.

Hence, by \autoref{std-eq},
\[
(\bullet) \otimes^{\mathbb{L}}_{KQ'} T \colon \der(KQ') \to \der(KQ)
\]
is a derived equivalence.
\end{example}

\begin{remark}
In the situation of \autoref{tilting-a-three}, the functor
\[
(\bullet) \otimes_{KQ'} T \colon
\operatorname{Mod}-KQ' \to \operatorname{Mod}-KQ
\]
is \emph{not} an equivalence of categories (\ie \emph{not} a Morita
equivalence in the classical sense).

Indeed, Morita equivalences preserve projective objects. Now $KQ'$ is a
projective $KQ$-module, but $T$ is not a projective $KQ$-module.
\end{remark}

Using the techniques we develeped, we can give a description of stable
categories of certain Frobenius categories as derived categories of certain
DG algebras.

\begin{theorem}
Let $\mathcal{A}$ be a ($k$-linear) Frobenius category which has
infinite direct sums and whose stable category admits
a compact generator $X$. Then there is a DG algebra $A$ and a
triangulated equivalence $\der A \to \underline{\mathcal{A}}$.
\end{theorem}

\begin{proof}
We will first replace $\mathcal{A}$ by a category in which endomorphisms
can be described via DG algebras: Let $\tilde{\mathcal{A}}$ be the category of
projective-injective ``resolution complexes''
\[
P = (\ldots \to P^{n} \xrightarrow{d^n} P^{n + 1} \to \ldots),
\]
\ie each object of $\tilde{\mathcal{A}}$ is obtained from an object
$Z$ of $\mathcal{A}$ by ``gluing'' a projective resolution
\[
\ldots \to Q^{-2} \xrightarrow{d^{-2}} Q^{-1} \xrightarrow{\varepsilon} Z
\]
to an injective resolution
\[
Z \xrightarrow{\eta} I^{1} \xrightarrow{d^1} I^2 \to \ldots
\]
and setting $d^0 = \eta \circ \varepsilon$.

If we equip $\tilde{\mathcal{A}}$ with the structure
of an exact category via componentwise split sequences,
the functor sending a resolution to its ``underlying object in $\mathcal{A}$''
induces a triangulated equivalence
$\underline{\tilde{\mathcal{A}}} \xrightarrow{\simeq}
\underline{\mathcal{A}}$.

Now  let $\tilde{X} \in \tilde{\mathcal{A}}$ be such a resolution of $X$ and
define $A \coloneqq \CEnd[\mathcal{A}]{\tilde{X}}$. Consider the functor
$\CHom[\mathcal{A}]{\tilde{X}}{\bullet} \colon \tilde{\mathcal{A}} \to \der A$,
which vanishes on projectives in $\tilde{\mathcal{A}}$ and hence
descends to a triangulated functor
$F \colon \underline{\tilde{\mathcal{A}}} \to \der A$.

Further, $F$ preserves with direct sums since there is a natural isomorphism
\[
H^n (\CHom[\mathcal{A}]{\tilde{X}}{\bullet}) \xrightarrow{\cong}
\Hom[\tilde{\mathcal{E}}]{\tilde{X}}{(\bullet)[n]}
\]
for all $n$ and the latter functor preserves direct sums as $\tilde{X}$ is
compact.

Now, similar to the last implication of \autoref{std-eq}, one can use
d\'evissage arguments on $\tilde{\mathcal{E}}$ to show that $F$ is fully
faithful and on $\der A$ to show that $F$ essentially surjective. Hence,
in total, we obtain a chain of triangulated equivalences
\[
\underline{\mathcal{E}} \xleftarrow{\simeq}
\underline{\tilde{\mathcal{E}}} \xrightarrow{\simeq}
\der A.
\]
\end{proof}

\bibliography{derived-morita}{}

\end{document}
