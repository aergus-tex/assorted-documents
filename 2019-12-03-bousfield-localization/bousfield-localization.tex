% A presentation about Bousfield's paper "The localization of spectra with
% respect to homology", created by Aras Ergus for the eCHT Kan seminar in fall
% 2019

% This work is licensed under a Creative Commons Attribution-ShareAlike 4.0
% International License.
% See the following website for details:
% https://creativecommons.org/licenses/by-sa/4.0/

% This file is based on the following template by Till Tantau:
% http://mirrors.ctan.org/macros/latex/contrib/beamer/doc/solutions/generic-talks/generic-ornate-15min-45min.en.tex

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\documentclass[aspectratio=169]{beamer}

\mode<presentation>
{
  \usetheme{Hannover}

  \setbeamercovered{transparent}

  % Display alerts as bold text (and not in red).
  \setbeamercolor{alerted text}{fg=}
  \setbeamerfont{alerted text}{series=\bfseries}
}

\usepackage[english]{babel}
\usepackage{microtype}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{csquotes}
\usepackage[
  type={CC},
  modifier={by-sa},
  version={4.0},
]{doclicense}

\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{bbm}
\usepackage{mathabx}
\usepackage{stmaryrd}
\usepackage{mathtools}
\usepackage{tikz-cd}
\usepackage{extarrows}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Math macros

% Functions.
\DeclareMathOperator{\Id}{Id}

% Number "domain"s.
\newcommand{\NN}{\mathbb{N}}
\newcommand{\QQ}{\mathbb{Q}}
\newcommand{\ZZ}{\mathbb{Z}}

% Spectra.
\newcommand{\Sp}{\mathsf{Sp}}
\newcommand{\sphere}{\mathbb{S}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Presentation preamble

\title[Localization of spectra]
{The localization of spectra with respect to homology}
\subtitle{by A.\ K.\ Bousfield}
\author{Aras Ergus}
\institute[EPFL]{École polytechnique fédérale de Lausanne (EPFL)}
\date[eKan]{eCHT Kan seminar, December 3, 2019}

\AtBeginSubsection[]
{
 \begin{frame}<beamer>{Outline}
   \tableofcontents[currentsection,currentsubsection]
 \end{frame}
}
\AtBeginSection[]
{
 \begin{frame}<beamer>{Outline}
   \tableofcontents[currentsection,currentsubsection]
 \end{frame}
}

% If you wish to uncover everything in a step-wise fashion, uncomment
% the following command:

% \beamerdefaultoverlayspecification{<+->}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% And now the actual presentation!

\begin{document}

\begin{frame}
  \titlepage
  \tiny{\doclicenseThis}
\end{frame}

\begin{frame}{Outline}
  \tableofcontents[pausesections]
\end{frame}

\begin{frame}{Conventions}
  The labeling of statements refers to the numbering in Bousfield's paper.
  \pause

  \vfill

  \begin{block}{Warning}
    What the category $\Sp$ of spectra is is intentionally kept vague.
    \pause

    Depending on whether $\Sp$ is the stable homotopy category, a point-set
    model or the $\infty$-category of spectra, the statements may mean slightly
    different things and may be stronger or weaker.
    \pause

    Bousfield uses the stable homotopy category and CW-spectra.
  \end{block}
  \pause

  \vfill

  \begin{block}{Assumption}
    The smash product $\wedge \colon \Sp \times \Sp \to \Sp$ is assumed to be
    \enquote{homotopically correct}, in particular exact in both variables.
  \end{block}
  \pause

  \vfill

  We fix a spectrum $E$ for the rest of the talk.
\end{frame}

\section{Localizations and acyclizations}

\subsection{Motivation}

\begin{frame}{$E$-equivalences}

  \begin{definition}
    A map $f \colon X \to Y$ of spectra is called an \alert{$E$-equivalence}
    if $E_* f \colon E_*X \to E_*Y$ is an isomorphism.
  \end{definition}
  \pause

  \vfill

  We would like to have have a category $\Sp_E$ equipped with a
  \enquote{localization functor} $(-)_E \colon \Sp \to \Sp_E$ s.t.\ %
  \pause
  \[
    \text{$f \colon X \to Y$ is an $E$-equivalence} \enskip \iff \enskip
    \text{$f_E \colon X_E \to Y_E$ is an equivalence.}
  \]
  \pause

  \begin{block}{Spoiler}
    In the end, we will be able to realize the target of the localization
    functor as the full subcategory of $\Sp$ consisting of \enquote{$E$-local}
    spectra.
  \end{block}
\end{frame}

\begin{frame}[fragile]{Why should we care about $E$-equivalences? (I)}
  \begin{example}[Proposition 2.9]
    Each spectrum $X$ sits in a homotopy pullback square
    \[
      \begin{tikzcd}
        X \ar{d} \ar{r} &
        \prod_{\text{$p$ prime}} X_{\sphere/p} \ar{d} \\
        X_{\sphere \QQ} \ar{r} &
        \left(\prod_{\text{$p$ prime}} X_{\sphere/p}\right)_{\sphere \QQ}
      \end{tikzcd},
    \]
    a.k.a.\ an \enquote{arithmetic square}.
  \end{example}
\end{frame}

\begin{frame}{Why should we care about $E$-equivalences? (II)}
  \begin{example}[a consequence of Theorem 6.6]
    Let $E$ be a connective ring spectrum such that $\pi_0 E \cong \ZZ / n$
    for some $n \geq 2$.
    \pause

    Let $Y$ be a connective spectrum with finitely generated homotopy groups.
    \pause

    Then the $E$-based Adams spectral sequence for $Y$ converges to
    $\pi_* Y_{\sphere/n}$.
  \end{example}
\end{frame}

\subsection{Formal properties}

\begin{frame}{$E$-acyclicity and $E$-locality}
  \begin{definition}
    A spectrum $X$ is called
    \pause
    \begin{itemize}
      \item \alert{$E$-acyclic} if $E_* X \cong 0$, i.e.\
        $E \wedge X \simeq 0$.
        \pause
      \item \alert{$E$-local} if for each $E$-equivalence $f \colon A \to B$,
        $f^* \colon [B, X]_\bullet \to [A, X]_\bullet$ is a bijection.
    \end{itemize}
  \end{definition}
  \pause

  \vfill

  \begin{lemma}
    A map $f \colon X \to Y$ is an $E$-equivalence if and only if its
    homotopy (co)fiber is $E$-acyclic.
  \end{lemma}
  \pause
  \begin{corollary}
    A spectrum $X$ is $E$-local iff for every $E$-acyclic spectrum $A$,
    $[A, X]_\bullet \cong 0$.
  \end{corollary}
\end{frame}

%\begin{frame}{Proof of the characterization of $E$-equivalences}
%  \begin{proof}
%    Extend $f$ to a cofiber sequence
%    \[
%      X \xrightarrow{f} Y \to C(f).
%    \]

%    Smashing with $E$ yields a cofiber sequence
%    \[
%      E \wedge X \xrightarrow{E \wedge f} E \wedge Y \to E \wedge C(f),
%    \]
%    so
%    \begin{align*}
%      & \text{$E_* f = \pi_* (E \wedge f)$ is an isomorphism} \\
%      \iff & \text{$E \wedge f$ is an equivalence} \\
%      \iff & \text{$C(E \wedge f) \simeq E \wedge C(f) \simeq 0$} \\
%      \iff & \text{$E_*(C(f)) \cong 0$}. \qedhere
%  \end{align*}
%  \end{proof}
%\end{frame}

\begin{frame}{A class of $E$-local spectra}
  \begin{lemma}[Lemma 1.3]
    If $E$ is a ring spectrum (up to homotopy), then all $E$-module spectra
    are $E$-local.
  \end{lemma}
  \pause

  \begin{proof}
    Let $A$ be an $E$-acyclic spectrum, $f \colon A \to X$.
    \pause

    Then, up to homotopy, $f$ can be factored as
    \[
      A \xrightarrow{1_E \wedge A} E \wedge A
      \xrightarrow{E \wedge f} E \wedge X
      \xrightarrow{\mathsf{act}_X} X.
    \]
    \pause

    Since $A$ is $E$-acyclic, $E \wedge A \simeq 0$.
    \pause

    Thus $f$ factors through $0$, so $f \sim 0$.
  \end{proof}
\end{frame}

\begin{frame}{$E$-equivalences between $E$-local spectra}
  \begin{fact}[Lemma 1.2, \enquote{$E$-Whitehead theorem}]
    Let $f \colon X \to Y$ be an $E$-equivalence between $E$-local spectra.

    Then $f$ is an equivalence.
  \end{fact}

%  \begin{proof}
%    By $E$-locality of $X$, the $E$-equivalence $f$ induces a bijection
%    $f^* \colon [Y, X] \to [X, X]$.

%    A preimage of $\Id_X$ yields a $g \colon Y \to X$ s.t.\
%    $Id_X \sim f^* g = g \circ f$.

%    By 2-out-of-3, $g \colon Y \to X$ is an $E$-equivalence.

%    Doing the same trick for $g$, we can obtain a left homotopy inverse
%    $f' \colon X \to Y$ for $g$: $f' \circ g \sim \Id_Y$.

%    Thus $g$ is a homotopy inverse for $f$.
%  \end{proof}
\end{frame}

\begin{frame}{Closure properties of $E$-local spectra}
  \begin{fact}[Lemmas 1.4-1.8]
    The subcategory $\Sp_E$ of $E$-local spectra is closed under
    \pause
    \begin{itemize}
      \item homotopy (co)fibers,
        \pause
      \item homotopy limits,
        \pause
      \item extensions,
        \pause
      \item retracts.
    \end{itemize}
  \end{fact}
  \pause

  \begin{block}{Remark}
    The dual statements hold for $E$-acyclic spectra.
  \end{block}
  \pause

  \begin{block}{Warning}
    In general, $\Sp_E$ is not closed under smash products.
  \end{block}
\end{frame}

\subsection{Existence}

\begin{frame}{The existence theorem}
  \begin{theorem}[Theorem 1.1]
    There are functors
    \pause
    \begin{itemize}
      \item ${_E(-)} \colon \Sp \to \Sp$ (\alert{$E$-acyclization}) which
        lands in $E$-acyclic spectra,
        \pause
      \item $(-)_E \colon \Sp \to \Sp$ (\alert{$E$-localization}) which lands
        in $E$-local spectra,
        \pause
    \end{itemize}
    such that for each spectrum $X$ there exists a natural homotopy (co)fiber
    sequence
    \pause
    \[
      {_EX} \xrightarrow{\theta_X} X \xrightarrow{\eta_X} X_E.
    \]
  \end{theorem}
\end{frame}

\newcommand{\LocalizationIntro}{%
  Have: hocofiber sequence
  ${_EX} \xrightarrow{\theta_X} X \xrightarrow{\eta_X} X_E$
  s.t.\ ${_EX}$ is $E$-acyclic and $X_E$ is $E$-local.
  \vfill
}

\begin{frame}{$\eta_E \colon X \to X_E$ is an $E$-equivalence.}

  \LocalizationIntro

  \begin{corollary}
    $\eta_X \colon X \to X_E$ is an $E$-equivalence.
  \end{corollary}
  \pause

  \begin{proof}
    Smashing the localization sequence with $E$ yields a homotopy (co)fiber
    sequence
    \[
      0 \simeq E \wedge {_EX} \xrightarrow{E \wedge \theta_X} E \wedge X
      \xrightarrow{E \wedge \eta_X} E \wedge X_E,
    \]
    \pause
    so $E_* \eta_X = \pi_* (E \wedge \eta_X)$ is an equivalence.
  \end{proof}
\end{frame}

\begin{frame}{Idempotency of the localization functor}

  \LocalizationIntro

  \begin{corollary}
    The functor $(-)_E \colon \Sp \to \Sp$ is idempotent (up to homotopy).
  \end{corollary}
  \pause

  \begin{proof}
    $\eta_{X_E} \colon X_E \to (X_E)_E$ is an $E$-equivalence between
    $E$-local spectra.
    \pause

    So it's an equivalence.
  \end{proof}
\end{frame}

\begin{frame}{%
  $(-)_E \colon \Sp \to \Sp_E$ is left adjoint to $\Sp_E \hookrightarrow \Sp$.
}

  \LocalizationIntro

  \begin{corollary}
    $\eta_X \colon X \to X_E$ is (up to homotopy) initial among maps from $X$
    to an $E$-local spectrum.
  \end{corollary}
  \pause

  \begin{proof}
    If $Y$ is $E$-local, then $\eta_X^* \colon [X, Y] \cong [X_E, Y]$
    since $\eta_E$ is an $E$-equivalence.
  \end{proof}
  \pause

  \vfill

  \begin{corollary}
    $E$-localization is left adjoint to the inclusion
    $\Sp_E \hookrightarrow \Sp$ of $E$-local spectra.
  \end{corollary}
\end{frame}

\begin{frame}{%
  ${_E(-)} \colon Sp \to {_E\Sp}$ is right adjoint to
  ${_E\Sp} \hookrightarrow \Sp$.
}
  \LocalizationIntro

  \begin{corollary}
    $\theta_X \colon {_EX} \to X$ is (up to homotopy) terminal among maps from
    an $E$-acyclic spectrum to $X$.
  \end{corollary}
  \pause

  \begin{corollary}
    $E$-acyclization is right adjoint to the inclusion
    $\Sp_E \hookrightarrow \Sp$ of $E$-acyclic spectra.
  \end{corollary}
\end{frame}

\begin{frame}{Exactness of acyclization and localization functors}
  \begin{corollary}
    $E$-acyclization and $E$-localization are exact functors.
  \end{corollary}
\end{frame}

\subsection{Construction}

\begin{frame}{How to construct localizations?}
  \begin{block}{Recipe for constructing $X_E$.}
    \pause
    \begin{enumerate}
      \item Construct a spectrum $aE$ such that $[A, Y]_\bullet \cong 0$
        for all $E$-acyclic $A$ iff $[aE, Y]_\bullet \cong 0$.
        \pause
      \item \enquote{Kill} all the maps from $aE$ to $X$.
    \end{enumerate}
  \end{block}

  \vfill
  \pause

  We'll sketch these constructions for CW-spectra (i.e.\ sequential
  spectra $(X_n)_{n \in \NN}$ s.t.\ every level $X_n$ is a CW-complex and
  the structure maps $\Sigma X_n \to \Sigma X_{n+1}$ are inclusions of
  subcomplexes).
\end{frame}

\begin{frame}{\enquote{The} acyclic spectrum}
  Fix an infinite cardinal $\sigma$ that is at least equal to
  $|\oplus_{n \in \ZZ} \pi_n E|$.

  \vfill
  \pause

  \begin{definition}
    Let $(K_i)_{i \in I}$ a system of representatives for the equivalence
    classes of $E$-acyclic spectra with at most $\sigma$ cells.
    \pause

    Set
    \[
      aE \coloneqq \bigvee_{i \in I} K_i.
    \]
  \end{definition}
\end{frame}

\begin{frame}{$aE$ \enquote{generates} all $E$-acyclic spectra (I)}
  Let $Y$ be a spectrum s.t.\ $[aE, Y]_\bullet \cong 0$.
  \pause

  We want to show that $[A, Y]_\bullet \cong 0$ for \alert{every} $E$-acyclic
  spectrum $A$.

  \vfill
  \pause

  To do that, we would like to construct a (transfinite) filtration
  \[
    0 = A_0 \subset A_1 \subset \ldots \subset A_\alpha = A
  \]
  by $E$-acyclic CW-subspectra s.t.\
  \pause
  \begin{enumerate}
    \item $A_{\gamma + 1} = A_\gamma \cup W_\gamma$ for a subspectrum
      $W_\gamma \subset A$ s.t.\
      \pause
      \begin{itemize}
        \item $W_\gamma \not \subset A_\gamma$,
          \pause
        \item $W_\gamma$ has at most $\sigma$ cells,
          \pause
        \item $E_*(A_{\gamma + 1}/ A_\gamma) \cong
          E_*(W_\gamma / (W_\gamma \cap A_\gamma)) \cong 0$.
      \end{itemize}
      \pause
    \item $A_\lambda = \bigcup_{i < \lambda} A_i$ for limit ordinals $\lambda$.
  \end{enumerate}
\end{frame}

\begin{frame}{$aE$ \enquote{generates} all $E$-acyclic spectra (II)}
  For a moment, assume that we do have filtration
  $0 = A_0 \subset A_1 \subset \ldots \subset A_\alpha = A$
  by $E$-acyclic CW-subspectra s.t.\
  \begin{enumerate}
    \item $A_{\gamma + 1} = A_\gamma \cup W_\gamma$ for a subspectrum
      $W_\gamma \subset A$ s.t.\
      \begin{itemize}
        \item $W_\gamma \not \subset A_\gamma$,
        \item $W_\gamma$ has at most $\sigma$ cells,
        \item $E_*(A_{\gamma + 1}/ A_\gamma) \cong
          E_*(W_\gamma / (W_\gamma \cap A_\gamma)) \cong 0$.
      \end{itemize}
    \item $A_\lambda = \bigcup_{i < \lambda} A_i$ for limit ordinals $\lambda$.
  \end{enumerate}
  \pause

  Note that the successor step guarantees that the subquotients
  $A_{\gamma + 1} / A_\gamma$ are $E$-acyclic spectra with at most
  $\sigma$ cells, so they are all \enquote{summands} of $aE = \bigvee_i K_i$.
  \pause

  Thus, by (transfinite) induction along this filtration, we can show that
  $[A , Y]_\bullet \cong 0$ if $[aE, Y]_\bullet \cong 0$.
\end{frame}

\begin{frame}{How to do the successor step?}
  \begin{lemma}
    Let $A$ be a CW-spectrum.
    \pause

    Let $B \subset A$ a proper closed subspectrum with $E_*(A/B) \cong 0$.
    \pause

    Let $e$ be a cell of $A$ that is not in $B$.
    \pause

    Then there exists a CW-subspectrum $W \subset A$ such that:
    \pause
    \begin{itemize}
      \item $W$ contains $e$.
        \pause
      \item $W$ has at most $\sigma$ cells.
        \pause
      \item $E_*(W / (W \cap B)) \cong 0$.
    \end{itemize}
  \end{lemma}
\end{frame}

\begin{frame}{Proof of the lemma needed for the successor step}
  \begin{proof}
    We will construct a sequence $(W_n)_{n \in \NN}$ of CW-subspectra such that
    \pause
    \begin{itemize}
      \item each $W_n$ contains $e$,
        \pause
      \item each $W_n$ has at most $\sigma$ cells,
        \pause
      \item $E_*(W_n / (W_n \cap B)) \to E_*(W_{n + 1} / (W_{n + 1} \cap B))$
        is zero for all $n$,
        \pause
    \end{itemize}
    and set $W \coloneqq \bigcup_n W_n$.
    \pause

    Let $W_0$ be a CW-subspectrum of $A$ with at most $\sigma$ cells that
    contains $e$.
    \pause

    Given $W_n$, consider $x \in E_*(W_n / (W_n \cap B))$.
    \pause
    As $E_*(A/B) \cong 0$, there exists a finite CW-subspectrum
    $F_x \subset X$ s.t.\ $x$ maps to $0$ in
    $E_*((W_n \cup F_x) / ((W_n \cup F_x) \cap B))$.
    \pause

    Let $W_{n + 1} \coloneqq W_n \cup \bigcup_{x} F_x$, which has at most
    $\sigma$ cells because there are at most $\sigma$ possibilities for $x$.
  \end{proof}
\end{frame}

\begin{frame}{Recap of the construction of $X_E$}
  We have constructed a spectrum $aE$ such that $[aE, Y]_\bullet \cong 0$
  iff $[A, Y]_\bullet \cong 0$ for every $E$-acyclic spectrum $A$.

  \vfill
  \pause

  Now we want to construct the $E$-localization $X_E$ of $X$ by
  \enquote{coning off} all maps from $aE$ to $X$.
\end{frame}

\begin{frame}{The small object argument}
  Given a (CW-)spectrum $X$, construct $X_E$ by (transfinite) induction as
  follows:
  \pause
  \begin{itemize}
    \item Let $X_0 \coloneqq X$.
      \pause
    \item Given $X_\alpha$, define $X_{\alpha + 1}$ to be the (homotopy)
      cofiber of
      \[
        \bigvee_{n \in \ZZ} \bigvee_{[f] \in [aE, X_\alpha]_n}
        S^i \xrightarrow{\bigvee_n \bigvee_{[f]} f} X_\alpha.
      \]
      \pause
    \item At limit ordinals $\lambda$ set
    $X_\lambda \coloneqq \operatorname{hocolim}_{i < \lambda} X_i$.
  \end{itemize}
  \pause

  Pick a cardinal $\kappa$ larger than the number of cells in $aE$.
  \pause
  Set $X_E \coloneqq X_\kappa$.
  \pause

  This guarantees that every map $\Sigma^i aE \to X_E$ factors through
  $X_i$ for some $i < \kappa$,
  \pause
  so is trivial because it gets coned off at the next stage.
\end{frame}

\subsection{The Bousfield lattice}


\begin{frame}{Bousfield classes}
  Let $F$ be another spectrum.

  \vfill
  \pause

  \begin{definition}
    $E$ and $F$ are called \alert{Bousfield equivalent} if one of the
    following equivalent conditions holds:
    \pause
    \begin{enumerate}[i]
      \item A spectrum is $E$-acyclic iff it is $F$-acyclic.
        \pause
      \item A map between spectra is an $E_*$-equivalence iff it is
        an $F_*$-equivalence.
    \end{enumerate}
    \pause

    The equivalence class of $E$ w.r.t.\ this relation will be called
    the \alert{Bousfield class of $E$} and denoted by $\langle E \rangle$.
  \end{definition}
\end{frame}

\begin{frame}{The set of Bousfield classes as a lattice}
  \begin{fact}
    The set (!) of Bousfield classes of spectra is a lattice with
    \pause
    \begin{itemize}
      \item join induced by wedge of spectra,
        \pause
      \item meet induced by smash product of spectra.
    \end{itemize}
    \pause

    In particular, $\langle 0 \rangle$ is the minimal element and
    $\langle \sphere \rangle$ is the maximal element.
  \end{fact}
  \pause

  \begin{definition}
    We define a partial order on the set of Bousfield clasess by declaring
    $\langle E \rangle \leq \langle F \rangle$ if every $F$-acyclic spectrum
    is $E$-acyclic.
  \end{definition}
  \pause

  \begin{block}{Remark}
    This order agrees with the one coming from the lattice structure.
  \end{block}
\end{frame}

\section{Localizations w.r.t.\ Moore spectra}

\begin{frame}{Acyclicity types of abelian groups}
  \begin{definition}
    Two abelian groups $G_1$ and $G_2$ have the \alert{same type of acyclicity}
    if
    \pause
    \begin{itemize}
      \item $G_1$ is a torsion group iff $G_2$ is, and
      \pause
      \item for each prime $p$, $G_1$ is uniquely $p$-divisible iff $G_2$ is.
    \end{itemize}
  \end{definition}
  \pause

  \begin{fact}[Proposition 2.3]
    For abelian groups $G_1$ and $G_2$, the following are equivalent:
    \pause
    \begin{enumerate}[i]
      \item $G_1$ and $G_2$ have the same type of acyclicity.
        \pause
      \item $\langle \sphere G_1 \rangle = \langle \sphere G_2 \rangle$.
        \pause
      \item $\sphere G_1$ and $\sphere G_2$ yield equivalent localization
        functors on $\Sp$.
    \end{enumerate}
  \end{fact}
\end{frame}

\begin{frame}{An explicit description of acyclicity types}
  \begin{block}{Remark}
    Every acyclicity class is represented by one of the following:
    \pause
    \begin{itemize}
      \item $\prod_{p \in J} \ZZ / p$ for a set $J$ of primes,
      \pause
      \item $\ZZ_{(J)}$ for a set $J$ of primes.
    \end{itemize}
  \end{block}
\end{frame}

\begin{frame}{Complements of acyclicity types (I)}
  \begin{definition}
    The \alert{complement} of an acyclicity type (or by abuse of terminology,
    an abelian group) is defined as follows:
    \pause
    \begin{itemize}
      \item If $\prod_{p \in J} \ZZ / p$ is in the class for a set $J$ of
        primes, then the complement contains $\ZZ_{(J)}$.
        \pause
      \item If $\ZZ_{(J)}$ is in the class for a set $J$ of
        primes, then the complement contains $\prod_{p \in J} \ZZ / p$.
    \end{itemize}
  \end{definition}
  \pause

  \begin{example}
    (The acyclicity classes of) $\QQ$ and $\prod_{\text{$p$ prime}} \ZZ/p$ are
    complements of each other.
  \end{example}

\end{frame}

\begin{frame}{Complements of acyclicity types (II)}
  \begin{block}{Remark}
    Let $G$ be an abelian group and $G'$ an abelian group in the complement
    of its acyclicity type.
    \pause

    Then:
    \begin{itemize}
      \item $G \oplus G'$ and $\ZZ$ have the same type of acyclicity.
        \pause
      \item
        $\langle \sphere G \vee \sphere G' \rangle = \langle \sphere \rangle$.
    \end{itemize}
  \end{block}
\end{frame}

\section{More cool results}

\begin{frame}[fragile]{The (generalized) arithmetic square}
  \begin{theorem}[Proposition 2.9]
    Each spectrum $X$ sits in a homotopy pullback square
    \[
      \begin{tikzcd}
        X_E \ar{d} \ar{r} &
        \prod_{\text{$p$ prime}} X_{E \wedge \sphere/p} \ar{d} \\
        X_{E \wedge\sphere \QQ} \ar{r} &
        \left(\prod_{\text{$p$ prime}}
        X_{E \wedge \sphere/p}\right)_{E \wedge \sphere \QQ}
      \end{tikzcd},
    \]
    where all the maps are induced by corresponding localizations.
  \end{theorem}
\end{frame}

\begin{frame}[fragile]{Proof of the arithmetic square theorem}
  \begin{proof}[Proof sketch]
    \[
      \begin{tikzcd}[row sep=huge, column sep=huge]
        X_E \ar{dr}
        \ar{ddr}[swap]{\text{$(E \wedge \sphere \QQ)$-eq.}}
        \ar{drr}{\text{$(E \wedge \sphere/p)$-eq. f.a.\ $p$}} & & \\
        & P
        \ar{d}{\text{$(E \wedge \sphere \QQ)$-eq.}}
        \ar{r}[swap]{\text{$(E \wedge \sphere/p)$-eq. f.a.\ $p$}}
        \arrow[dr, phantom, "\lrcorner" , very near start, color=black] &
        \prod_{\text{$p$ prime}} X_{E \wedge \sphere/p} \ar{d} \\
        & X_{E \wedge\sphere \QQ} \ar{r} &
        \left(\prod_{\text{$p$ prime}}
        X_{E \wedge \sphere/p}\right)_{E \wedge \sphere \QQ}
      \end{tikzcd},
    \]

    The homotopy pullback $P$ is $E$-local as a limit of $E$-local spectra,
    so it's enough to show that $X_E \to P$ is an $E$-equivalence.
  \end{proof}
\end{frame}

\begin{frame}{Localizations of connective spectra w.r.t.\ connective spectra}
  \begin{theorem}[Theorem 3.1]
    Assume that $E$ is connective.
    \pause

    Let $X$ be a connective spectrum.
    \pause

    Then
    $X_E \simeq X_{\sphere (\oplus_{n \in \ZZ} \pi_n E)}$.
  \end{theorem}
  \pause

  \begin{corollary}
    Let $G$ be an abelian group, $X$ a connective spectrum.
    \pause

    Then $X_{H G} \simeq X_{\sphere G}$.
  \end{corollary}
\end{frame}

\begin{frame}{A \enquote{telescope theorem}}
  \begin{theorem}[Proposition 4.2]
  Let $p$ be a prime number.
  \pause

  Let $A_p \colon \Sigma^{2(p-1)} \sphere/p \to \sphere/p$ for $p$ odd resp.\ %
  $A_p \colon \Sigma^8 \sphere/2 \to \sphere/2$ for $p = 2$ be the Adams map.
  \pause

  Then the natural map
  \[
    \sphere/p \to \operatorname{hocolim}(\sphere/p
    \xrightarrow{\Sigma^{- \deg A_p} A_p} \Sigma^{- \deg A_p} \sphere/p
    \xrightarrow{\Sigma^{- 2 \deg A_p} A_p} \ldots)
  \]
  is a $KU$-localization.
  \end{theorem}
\end{frame}

\end{document}

