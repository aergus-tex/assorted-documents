% Notes computing the simplicial homology of the real projective plane and the
% Klein bottle, formerly model solutions for a course on algebraic topology
% Written by Aras Ergus.

% This work is licensed under a Creative Commons Attribution-ShareAlike 4.0
% International License.
% See the following website for details:
% https://creativecommons.org/licenses/by-sa/4.0/

\documentclass[a4paper, 12pt]{scrartcl}

\usepackage[
  type={CC},
  modifier={by-sa},
  version={4.0},
]{doclicense}

\usepackage{n002-math}
\usepackage{n002-things}

\usepackage{tikz}
\usetikzlibrary{decorations.markings,positioning,cd}

\usepackage{relsize}

\title{Simplicial homology of the real projective plane and the Klein bottle}
\author{Aras Ergus}
\date{June 27, 2019}

\begin{document}

\maketitle

\begin{abstract}
This document contains two examples of simplicial homology computations.
They were originally written as model solutions for some exercises in a
course on algebraic topology given by Kathryn Hess and showcase some
techniques (such as the Mayer--Vietoris sequence and excision) that were
introduced in the class around the time those exercises appeared.
\end{abstract}

\doclicenseThis

\tableofcontents

\section{The real projective plane}

In this section, we will find a(n abstract) simplicial complex $K$ whose
realization is homeomorphic to the real projective plane $\RR P^2$ and compute
its simplicial homology using a Mayer--Vietoris sequence.

In the following, we freely use some usual conventions and abuses of notation
such as representing abstract simplicial complexes and their labelings
with pictures, using the alphabetical ordering of the vertices for
homology calculations, writing $v$ instead of $\{v\}$ for a $0$-simplex \etc

Fixing possible sign issues is left as an exercise to the reader.

\subsection*{The complex and the decomposition}

Here is a possible way to realize $\RR P^2$ as a simplicial complex:\footnote{
There is actually a simplicial complex with only ten $2$-simplicies that
realizes $\RR P^2$, but we'll stick to the one above because it's somewhat
more straightforward to come up with:
One can obtain $\RR P^2$ from a square by identifying ``antipodal'' points
of its boundary, and inspired by how one realizes the cylinder as a simplicial
complex, one can subdivide that square into three ``layers'' vertically
and horizontally to obtain the simplicial complex above.
}

\[
K :
\begin{tikzpicture}[baseline=-1.6cm, node distance=1cm]
\coordinate[label=above left:$a$] (a1) {};
\coordinate[below=of a1,label=left:$b$] (b1) {};
\coordinate[below=of b1,label=left:$c$] (c1) {};
\coordinate[below=of c1,label=below left:$d$] (d1) {};
\coordinate[right=of d1,label=below:$e$] (e1) {};
\coordinate[right=of e1,label=below:$f$] (f1) {};
\coordinate[right=of f1,label=below right:$a$] (a2) {};
\coordinate[above=of a2,label=right:$b$] (b2) {};
\coordinate[above=of b2,label=right:$c$] (c2) {};
\coordinate[above=of c2,label=above right:$d$] (d2) {};
\coordinate[left=of d2,label=above:$e$] (e2) {};
\coordinate[left=of e2,label=above:$f$] (f2) {};
\coordinate[below=of f2,label=below right:$g$] (g) {};
\coordinate[below=of g,label=below right:$h$] (h) {};
\coordinate[right=of h,label=above left:$i$] (i) {};
\coordinate[above=of i,label=above left:$j$] (j) {};
\draw (a1) -- (b1);
\draw (b1) -- (c1);
\draw (c1) -- (d1);
\draw (d1) -- (e1);
\draw (e1) -- (f1);
\draw (f1) -- (a2);
\draw (a2) -- (b2);
\draw (b2) -- (c2);
\draw (c2) -- (d2);
\draw (d2) -- (e2);
\draw (e2) -- (f2);
\draw (f2) -- (a1);
\draw (f2) -- (g);
\draw (g) -- (h);
\draw (h) -- (e1);
\draw (e2) -- (j);
\draw (j) -- (i);
\draw (i) -- (f1);
\draw (b1) -- (g);
\draw (g) -- (j);
\draw (j) -- (c2);
\draw (c1) -- (h);
\draw (h) -- (i);
\draw (i) -- (b2);
\draw (a1) -- (g);
\draw (c1) -- (g);
\draw (d1) -- (h);
\draw (g) -- (e2);
\draw (h) -- (j);
\draw (e1) -- (i);
\draw (j) -- (d2);
\draw (i) -- (c2);
\draw (i) -- (a2);
\end{tikzpicture}.
\]

To use the Mayer--Vietoris sequence, we want to write $K$ as the union of
two subcomplexes whose homology (and that of their intersection) we know well
(or can compute easily).
One of the many possibilities to do this is as follows:

\[
K =
\substack{
\left(
\begin{tikzpicture}[baseline=-1.3cm,node distance=0.9cm]
\coordinate[label=left:$f$] (f2) {};
\coordinate[right=of f2,label=right:$e$] (e2) {};
\coordinate[below=of f2,label=left:$g$] (g) {};
\coordinate[below=of g,label=left:$h$] (h) {};
\coordinate[right=of h,label=right:$i$] (i) {};
\coordinate[above=of i,label=right:$j$] (j) {};
\coordinate[below=of h,label=left:$e$] (e1) {};
\coordinate[below=of i,label=right:$f$] (f1) {};
\draw (e1) -- (f1);
\draw (e2) -- (f2);
\draw (f2) -- (g);
\draw (g) -- (h);
\draw (h) -- (e1);
\draw (e2) -- (j);
\draw (j) -- (i);
\draw (i) -- (f1);
\draw (g) -- (j);
\draw (h) -- (i);
\draw (g) -- (e2);
\draw (h) -- (j);
\draw (e1) -- (i);
\end{tikzpicture}
\right) \\
\mathlarger{K_1}
}
\mathlarger{\mathlarger{\cup}}
\substack{
\left(
\begin{tikzpicture}[baseline=-1.3cm,node distance=0.9cm]
\coordinate[label=left:$a$] (a1) {};
\coordinate[below=of a1,label=left:$b$] (b1) {};
\coordinate[below=of b1,label=left:$c$] (c1) {};
\coordinate[below=of c1,label=left:$d$] (d1) {};
\coordinate[right=of d1,label=right:$e$] (e1) {};
\coordinate[right=of e1,label=left:$f$] (f1) {};
\coordinate[right=of f1,label=right:$a$] (a2) {};
\coordinate[above=of a2,label=right:$b$] (b2) {};
\coordinate[above=of b2,label=right:$c$] (c2) {};
\coordinate[above=of c2,label=right:$d$] (d2) {};
\coordinate[left=of d2,label=left:$e$] (e2) {};
\coordinate[left=of e2,label=right:$f$] (f2) {};
\coordinate[below=of f2,label=right:$g$] (g) {};
\coordinate[below=of g,label=right:$h$] (h) {};
\coordinate[right=of h,label=left:$i$] (i) {};
\coordinate[above=of i,label=left:$j$] (j) {};
\draw (a1) -- (b1);
\draw (b1) -- (c1);
\draw (c1) -- (d1);
\draw (d1) -- (e1);
\draw (f1) -- (a2);
\draw (a2) -- (b2);
\draw (b2) -- (c2);
\draw (c2) -- (d2);
\draw (d2) -- (e2);
\draw (f2) -- (a1);
\draw (f2) -- (g);
\draw (g) -- (h);
\draw (h) -- (e1);
\draw (e2) -- (j);
\draw (j) -- (i);
\draw (i) -- (f1);
\draw (b1) -- (g);
\draw (j) -- (c2);
\draw (c1) -- (h);
\draw (i) -- (b2);
\draw (a1) -- (g);
\draw (c1) -- (g);
\draw (d1) -- (h);
\draw (j) -- (d2);
\draw (i) -- (c2);
\draw (i) -- (a2);
\end{tikzpicture}
\right)
\\
\mathlarger{K_2}
}.
\]

\subsection*{First subcomplex}

Note that $K_1$ is a representation of the M\"obius strip.
You may have seen that its homology is isomorphic to that of
$\partial \Delta^2$, but we will compute it to have another demonstration
of how to use the Mayer--Vietoris sequence and because it will be important to
know an explicit generator of
$H_1(K_1) \cong H_1(\partial \Delta^2) \cong \ZZ$.

Here is the decomposition of $K_1$ that we will use to compute its
homology:
\[
K_1 =
\substack{
\left(
\begin{tikzpicture}[baseline=-1.3cm,node distance=0.9cm]
\coordinate (f2) {};
\coordinate[right=of f2] (e2) {};
\coordinate[below=of f2,label=left:$g$] (g) {};
\coordinate[below=of g,label=left:$h$] (h) {};
\coordinate[right=of h,label=right:$i$] (i) {};
\coordinate[above=of i,label=right:$j$] (j) {};
\coordinate[below=of h] (e1) {};
\coordinate[below=of i] (f1) {};
\draw (g) -- (h);
\draw (j) -- (i);
\draw (g) -- (j);
\draw (h) -- (i);
\draw (h) -- (j);
\end{tikzpicture}
\right) \\
\mathlarger{K_{1, 1}}
}
\mathlarger{\mathlarger{\cup}}
\substack{
\left(
\begin{tikzpicture}[baseline=-1.3cm,node distance=0.9cm]
\coordinate[label=left:$f$] (f2) {};
\coordinate[right=of f2,label=right:$e$] (e2) {};
\coordinate[below=of f2,label=left:$g$] (g) {};
\coordinate[below=of g,label=left:$h$] (h) {};
\coordinate[right=of h,label=right:$i$] (i) {};
\coordinate[above=of i,label=right:$j$] (j) {};
\coordinate[below=of h,label=left:$e$] (e1) {};
\coordinate[below=of i,label=right:$f$] (f1) {};
\draw (e1) -- (f1);
\draw (e2) -- (f2);
\draw (f2) -- (g);
\draw (h) -- (e1);
\draw (e2) -- (j);
\draw (i) -- (f1);
\draw (g) -- (j);
\draw (h) -- (i);
\draw (g) -- (e2);
\draw (e1) -- (i);
\end{tikzpicture}
\right) \\
\mathlarger{K_{1, 2}}
}.
\]

Note that $K_{1,1}$ is the union of two copies of $\Delta^2$ whose intersection
is isomorphic to $\Delta^1$.
Thus it is acyclic as the union of two acyclic subcomplexes whose intersection
is also acyclic.

Doing the identifications given by the labeling, we see that $K_{1,2}$ can be
written as the union of two copies of $K_{1,1}$ whose intersection is
isomorphic to $\Delta^1$:
\[
K_{1,2}:
\begin{tikzpicture}[baseline=-1.1cm,node distance=1cm]
\coordinate (i) {};
\coordinate[label=left:$i$] (i) {};
\coordinate[right=of i,label=right:$h$] (h) {};
\coordinate[below=of i,label=left:$f$] (f) {};
\coordinate[right=of f,label=right:$e$] (e) {};
\coordinate[below=of f,label=left:$g$] (g) {};
\coordinate[below=of e,label=right:$j$] (j) {};
\fill[opacity = 0.2] (e) -- (f) -- (g) -- (j);
\fill[opacity = 0.6] (e) -- (f) -- (i) -- (h);
\draw[line cap=butt,line width=0.75mm] (e) -- (f);
\draw (f) -- (g);
\draw (h) -- (e);
\draw (e) -- (j);
\draw (i) -- (f);
\draw (g) -- (j);
\draw (h) -- (i);
\draw (g) -- (e);
\draw (e) -- (i);
\end{tikzpicture}.
\]
Hence $K_{1,2}$ is also acyclic.

The intersection $K_{1,1} \cap K_{1,2}$ consists of two disjoint copies of
$\Delta^1$ (namely those spanned by $\{g, j\}$ and $\{h, i\}$).
Thus $H_n(K_{1,1} \cap K_{1,2}) \cong 0$ for $n > 0$ and
$H_0(K_{1,1} \cap K_{1,2}) \cong \ZZ^2$ is a free abelian group on the
generators $[g]$ and $[h]$.

We can now compute the homology of $K_1$. First we note that $K_1$ is
connected, so $H_0(K_1) \cong \ZZ$ (which can also be seen from the
Mayer--Vietoris sequence).

To calculate $H_1(K_1)$, we have a look at the corresponding segment of the
Mayer--Vietoris sequence:
\[
0 \cong H_1(K_{1,1}) \oplus H_1(K_{1,2}) \to
H_1(K_1) \xrightarrow{\partial_1}
H_0(K_{1,1} \cap K_{1,2}) \xrightarrow{\phi_0}
H_0(K_{1,1}) \oplus H_0(K_{1,2}).
\]
This means that $\partial_1$ is injective and thus an isomorphism onto its
image $\im \partial_1 = \ker \phi_0$.

To determine $\ker \phi_0$, we note that $[h] = [g]$ in $H_0(K_{1,1})$ and
$H_0(K_{1,2})$, so
\[
  \phi_0(m [g] + n [h]) = ( (m + n)[g], -(m + n)[g]) \in
  H_0(K_{1,1}) \oplus H_0(K_{1,2})
\]
which is zero if and only if $m = -n$.
Hence we have
\[
  \ker \phi_0 = \class{-k[g] + k[h]}{k \in \ZZ} = \ZZ \cdot ([h] - [g])
  \subseteq H_0(K_{1,1} \cap K_{1,2}) = \ZZ \cdot [g] \oplus \ZZ \cdot [h].
\]
Thus $H_1(K_1) \cong \ZZ$ and the preimage of $[h]-[g]$ under $\partial_1$
is a generator.

Intuitively speaking, this preimage is represented by two sequences of
edges connecting $g$ and $h$ in $K_{1,1}$ \resp $K_{1,2}$ such that
their union is a cycle in $K_1$.
An example of this would be taking $(\{g, h\})$ in $K_{1,1}$ and
$(\{f, g\}, \{e, f\}, \{e, h\})$ in  $K_{1,2}$, which would yield the
generator $[\{f,g\} + \{g,h\} - \{e, h\} + \{e, f\}] \in H_1(K_1)$ after
choosing appropriate signs.

In order to be more precise about this, we have to recall how $\partial_1$ is
defined using the following diagram:
\[
\begin{tikzcd}
0 \ar{r} &
C_1(K_{1,1} \cap K_{1,2}) \ar{r}{\varphi_1}
\ar{d}{d^{K_{1,1} \cap K_{1,2}}_1} &
C_1(K_{1,1}) \oplus C_1(K_{1,2}) \ar{r}{\varrho_1}
\ar{d}{d^{K_{1,1}}_1 \oplus d^{K_{1,2}}_1} &
C_1(K_1) \ar{r} \ar{d}{d^{K_1}_1} &
0 \\
0 \ar{r} &
C_0(K_{1,1} \cap K_{1,2}) \ar[swap]{r}{\varphi_0} &
C_0(K_{1,1}) \oplus C_0(K_{1,2}) \ar[swap]{r}{\varrho_0} &
C_0(K_1) \ar{r} &
0 \\
\end{tikzcd}.
\]
Namely, given a $1$-cycle $\eta$ in $K_1$, one
lifts $\eta$ along $\varrho_1$, checks that the image of the lift
under $d^{K_{1,1}}_1 \oplus d^{K_{1,2}}_1$ comes from a $0$-cycle $\eta'$ in
$C_0(K_{1,1} \cap K_{1,2})$ and sets $\partial_1([\eta])$ to be
$[\eta'] \in H_0(K_{1,1} \cap K_{1,2})$.

Hence, if we can find $\alpha \in C_1(K_{1,2})$ and $\beta \in C_1(K_{1,2})$
such that $(d^{K_{1,1}}_1(\alpha), d^{K_{1,2}}_1(\beta)) =
(h - g, g - h) = \varphi_0(h - g)$ and $\gamma \coloneqq
\varrho_1(\alpha, \beta) = \alpha + \beta \in \ker d^{K_1}_1$, we will have
$\partial_1([\gamma]) = [h] - [g]$, which means that
$[\gamma]$ is a generator of $H_1(K_1)$.

To realize the example from above, we set $\alpha = \{g, h\}$ and
$\beta = -\{e, h\} + \{e, f\} + \{f,g\}$. Then we indeed have
$d^{K_{1,1}}_1(\alpha) = h - g$ and $d^{K_{1,2}}_1(\beta) =
e - h + f - e + g - f = g - h$.
Moreover, a straightforward calculation shows that $\gamma \coloneqq
\varrho_1(\alpha, \beta) = \{f, g\} + \{g, h\} - \{e, h\} + \{e, f\}$
is a cycle, so $[\gamma] = [\{f,g\} + \{g,h\} - \{e, h\} + \{e, f\}]$ is
indeed a generator of $H_1(K_1)$.

Next, we see that $H_2(K_1)$ is ``squeezed between trivial groups''
in the MV sequence:
\[
0 \cong H_2(K_{1,1}) \oplus H_2(K_{1,2}) \to
H_2(K_1) \to
H_1(K_{1,1} \cap K_{1,2}) \cong 0,
\]
so it also is trivial.
Moreover, $H_n(K_1) \cong 0$ for $n > 2$ as $K_1$ is a $2$-dimensional
complex.

All in all, we have calculated that
\[
H_n(K_1) \cong
\begin{cases}
\ZZ & n \in \{0, 1\} \\
0 & \text{otherwise}
\end{cases}
\]
where $H_1(K_1) = \ZZ \cdot [\{f,g\} + \{g,h\} - \{e, h\} + \{e, f\}]$.

\subsection*{Second subcomplex}

After doing the identifications given by the labeling, $K_2$ looks as
follows:
\[
K_2 :
\begin{tikzpicture}[baseline=-3.3cm,node distance=1.25cm]
\coordinate[label=above:$f$] (f) {};
\coordinate[below=of f,label=above left:$a$] (a) {};
\coordinate[below=of a,label=below left:$b$] (b) {};
\coordinate[below=of b,label=below left:$c$] (c) {};
\coordinate[below=of c,label=below left:$d$] (d) {};
\coordinate[below=of d,label=below:$e$] (e) {};
\coordinate[left=of b,label=left:$i$] (i) {};
\coordinate[left=of c,label=left:$j$] (j) {};
\coordinate[right=of b,label=right:$g$] (g) {};
\coordinate[right=of c,label=right:$h$] (h) {};
\draw (a) -- (b);
\draw (b) -- (c);
\draw (c) -- (d);
\draw (d) -- (e);
\draw (f) -- (a);
\draw (f) -- (g);
\draw (g) -- (h);
\draw (h) -- (e);
\draw (e) -- (j);
\draw (j) -- (i);
\draw (i) -- (f);
\draw (b) -- (g);
\draw (j) -- (c);
\draw (c) -- (h);
\draw (i) -- (b);
\draw (a) -- (g);
\draw (c) -- (g);
\draw (d) -- (h);
\draw (j) -- (d);
\draw (i) -- (c);
\draw (i) -- (a);
\end{tikzpicture}.
\]

The picture makes it evident that $|K_2|$ is homeomorphic to a disk and
we will show that $K_2$ is indeed acyclic by decomposing it into two
acyclic subcomplexes whose intersection is acyclic.

First we note that the simplicial complex
\[
L :
\begin{tikzpicture}[baseline=-0.35cm,node distance=0.5cm]
\coordinate[label=above:$u$] (u) {};
\coordinate[below right=of u,label=below:$v$] (v) {};
\coordinate[above right=of v,label=above:$w$] (w) {};
\draw (u) -- (v);
\draw (v) -- (w);
\end{tikzpicture}
\]
is acyclic because it is the union of two copies of $\Delta^1$ whose
intersection is isomorphic to $\Delta^0$.

Now we can iteratively build $K_2$ by starting with a complex isomorphic
to the acyclic complex $K_{1,1}$ from above and in each step adding a copy of
$K_{1,1}$ in a way that the intersection is isomorphic to
$\Delta^1$ or $L$ (thus also acyclic), which means that each complex in
the sequence is acyclic:
\[
\begin{tikzpicture}[baseline=-2cm,node distance=0.75cm]
\coordinate (f) {};
\coordinate[below=of f] (a) {};
\coordinate[below=of a] (b) {};
\coordinate[below=of b] (c) {};
\coordinate[below=of c] (d) {};
\coordinate[below=of d] (e) {};
\coordinate[left=of b] (i) {};
\coordinate[left=of c] (j) {};
\coordinate[right=of b] (g) {};
\coordinate[right=of c] (h) {};
\clip (h) -- (e) -- (j);
\fill[opacity = 0.3] (d) -- (h) -- (e) -- (j);
\fill[opacity = 0.7] (d) -- (j) -- (h);
\draw[line cap=round,line width=0.5mm] (d) -- (j);
\draw[line cap=round,line width=0.5mm] (d) -- (h);
\end{tikzpicture}
\leadsto
\begin{tikzpicture}[baseline=-2cm,node distance=0.75cm]
\coordinate (f) {};
\coordinate[below=of f] (a) {};
\coordinate[below=of a] (b) {};
\coordinate[below=of b] (c) {};
\coordinate[below=of c] (d) {};
\coordinate[below=of d] (e) {};
\coordinate[left=of b] (i) {};
\coordinate[left=of c] (j) {};
\coordinate[right=of b] (g) {};
\coordinate[right=of c] (h) {};
\clip (h) -- (e) -- (j) -- (i) -- (b) -- (c);
\fill[opacity = 0.3] (h) -- (e) -- (j);
\fill[opacity = 0.7] (b) -- (c) -- (j) -- (i);
\draw[line cap=butt,line width=0.5mm] (c) -- (j);
\end{tikzpicture}
\leadsto
\begin{tikzpicture}[baseline=-2cm,node distance=0.75cm]
\coordinate (f) {};
\coordinate[below=of f] (a) {};
\coordinate[below=of a] (b) {};
\coordinate[below=of b] (c) {};
\coordinate[below=of c] (d) {};
\coordinate[below=of d] (e) {};
\coordinate[left=of b] (i) {};
\coordinate[left=of c] (j) {};
\coordinate[right=of b] (g) {};
\coordinate[right=of c] (h) {};
\clip (h) -- (e) -- (j) -- (i) -- (b) -- (g);
\fill[opacity = 0.3] (h) -- (e) -- (j) -- (i) -- (b) -- (c);
\fill[opacity = 0.7] (c) -- (b) -- (g) -- (h);
\draw[line cap=round,line width=0.5mm] (b) -- (c);
\draw[line cap=round,line width=0.5mm] (c) -- (h);
\end{tikzpicture}
\leadsto
\begin{tikzpicture}[baseline=-2cm,node distance=0.75cm]
\coordinate (f) {};
\coordinate[below=of f] (a) {};
\coordinate[below=of a] (b) {};
\coordinate[below=of b] (c) {};
\coordinate[below=of c] (d) {};
\coordinate[below=of d] (e) {};
\coordinate[left=of b] (i) {};
\coordinate[left=of c] (j) {};
\coordinate[right=of b] (g) {};
\coordinate[right=of c] (h) {};
\clip (h) -- (e) -- (j) -- (i) -- (a) -- (g);
\fill[opacity = 0.3] (h) -- (e) -- (j) -- (i) -- (b) -- (g);
\fill[opacity = 0.7] (a) -- (i) -- (g);
\draw[line width=0.5mm] (i) -- (g);
\end{tikzpicture}
\leadsto
\begin{tikzpicture}[baseline=-2cm,node distance=0.75cm]
\coordinate (f) {};
\coordinate[below=of f] (a) {};
\coordinate[below=of a] (b) {};
\coordinate[below=of b] (c) {};
\coordinate[below=of c] (d) {};
\coordinate[below=of d] (e) {};
\coordinate[left=of b] (i) {};
\coordinate[left=of c] (j) {};
\coordinate[right=of b] (g) {};
\coordinate[right=of c] (h) {};
\clip (f) -- (i) -- (j) -- (e) -- (h) -- (g);
\fill[opacity = 0.3] (h) -- (e) -- (j) -- (i) -- (a) -- (g);
\fill[opacity = 0.7] (a) -- (i) -- (f) -- (g);
\draw[line cap=round,line width=0.5mm] (i) -- (a);
\draw[line cap=round,line width=0.5mm] (a) -- (g);
\end{tikzpicture}.
\]

\subsection*{The intersection and the final MV sequence}

The intersection $K_1 \cap K_2$ is given by
\[
K_1 \cap K_2:
\begin{tikzpicture}[baseline=-1.5cm,node distance=1cm]
\coordinate[label=left:$f$] (f2) {};
\coordinate[right=of f2,label=right:$e$] (e2) {};
\coordinate[below=of f2,label=left:$g$] (g) {};
\coordinate[below=of g,label=left:$h$] (h) {};
\coordinate[right=of h,label=right:$i$] (i) {};
\coordinate[above=of i,label=right:$j$] (j) {};
\coordinate[below=of h,label=left:$e$] (e1) {};
\coordinate[below=of i,label=right:$f$] (f1) {};
\draw (f2) -- (g);
\draw (g) -- (h);
\draw (h) -- (e1);
\draw (e2) -- (j);
\draw (j) -- (i);
\draw (i) -- (f1);
\end{tikzpicture}
\]
which represents a hexagon with vertices $f$, $g$, $h$, $e$, $j$, $i$ after
doing the identifications indicated by the labeling.

We refrain from computing its homology here which can be done directly
or using a Mayer--Vietoris sequence.
The result is
\[
H_n(K_1 \cap K_2) \cong
\begin{cases}
\ZZ & n \in \{0, 1\} \\
0 & \text{otherwise}
\end{cases}
\]
where $H_1(K_1 \cap K_2)$ is generated by the class of
$\theta \coloneqq \{f,g\} + \{g,h\} - \{e, h\} +
\{e, j\} - \{i, j\} - \{f, i\}$, \ie a generating cycle is given by
``going around the circle once''.

Now we start computing the homology of $K$.
Since $K$ is connected, $H_0(K) \cong \ZZ$.

To compute $H_1(K)$, we will analyze the following segment of the
Mayer--Vietoris sequence:
\[
H_1(K_1 \cap K_2) \xrightarrow{\phi_1}
H_1(K_1) \oplus H_1(K_2) \xrightarrow{\rho_1}
H_1(K) \xrightarrow{\partial_1}
H_0(K_1 \cap K_2) \xrightarrow{\phi_0}
H_0(K_1) \oplus H_0(K_2).
\]

Using the homology class of $g$ as a generator of $0$-th homology
groups of $K_1$, $K_2$ and $K_1 \cap K_2$, we see that
\begin{align*}
\ZZ \cong H_0(K_1 \cap K_2) & \xrightarrow{\phi_0}
H_0(K_1) \oplus H_0(K_2) \cong \ZZ^2 \\
m [g] & \mapsto (m [g], -m [g])
\end{align*}
is injective, \ie $\ker \phi_0 = 0$.

Hence, by exactness, $\im \partial_1 = \ker \phi_0 = 0$.
This yields, again by exactness, $H_1(K) = \ker \partial_1 = \im \rho_1$.
Using that $\ker \rho_1 = \im \phi_1$, this means that
$H_1(K) \cong \operatorname{coker} \phi_1$ by the first isomorphism theorem.

Now $\phi_1$ is a homomorphism
\[
\ZZ \cong H_1(K_1 \cap K_2) \to
H_1(K_1) \oplus H_1(K_2) \cong H_1(K_1) \oplus 0 \cong \ZZ,
\]
so it maps the generator
$[\theta] = [\{f,g\} + \{g,h\} - \{e, h\} + \{e, j\} - \{i, j\} - \{f, i\}]$
of $H_1(K_1 \cap K_2)$ to a multiple $k \cdot [\gamma]$ of the generator
$[\gamma] = [\{f,g\} + \{g,h\} - \{e, h\} + \{e, f\}]$ of $H_1(K_1)$
and thus its image is $k \cdot \ZZ \cdot [\gamma]$, which means that
its cokernel is isomorphic to $\ZZ / k\ZZ$.

Intuitively speaking, one can say that
``the cycle $\{f,g\} + \{g,h\} - \{e, h\} + \{e, j\} - \{i, j\} - \{f, i\}$
goes around the M\"obius strip $K_1$ twice'', so $k$ must be 2.
This can be made precise as follows:

Let $\gamma' \coloneqq \{e,j\} - \{i, j\} - \{f, i\} - \{e, f\} \in
C_1(K_1 \cap K_2) \subseteq C_1(K_1)$.
Note that $\gamma'$ is a cycle in $K_1$ and that $\theta = \gamma + \gamma'$,
so $[\theta] = [\gamma] + [\gamma']$ in $H_1(K_1)$.
Therefore it is enough to show that $[\gamma] = [\gamma']$, \ie
$[\gamma - \gamma'] = 0$, in $H_1(K_1)$.
Also this has a geometric interpretation:
In the representation of $K_1$ as a rectangle whose top and bottom edge are
appropriately identified, $\gamma - \gamma'$ corresponds to the boundary
of the rectangle, so it is the image of an appropriate sum of
the $2$-simplices in the rectangle under $d^{K_1}_2$:
\begin{align*}
= & d^{K_1}_2( \{e, f, g\} + \{e, g, j\} + \{g, h, j\} + \{h, i, j\} -
\{e, h, i\} + \{e, f, i\}) \\
= & (\{f, g\} - \{e, g\} + \{e, f\}) +
(\{g, j\} - \{e, j\} + \{e, g\}) +
(\{h, j\} - \{g, j\} + \{g, h\}) + \\
& \quad (\{i, j\} - \{h, j\} + \{h, i\}) -
(\{h, i\} - \{e, i\} + \{e, h\}) +
(\{f, i\} - \{e, i\} + \{e, f\}) \\
= & (\{f, g\} + \{e, f\}) +
(- \{e, j\}) +
(\{g, h\}) +
(\{i, j\}) -
(\{e, h\}) +
(\{f, i\} + \{e, f\}) \\
= & (\{f, g\} + \{g, h\} - \{e, h\} + \{e, f\}) +
(-\{e, j\} + \{i, j\} + \{f, i\} + \{e, f\}) \\
= & \gamma - \gamma'.
\end{align*}

Hence $\phi_1([\theta])$ indeed corresponds to $[\gamma'] + [\gamma] =
[\gamma] + [\gamma] = 2 [\gamma]$, so $H_1(K) \cong \ZZ / 2 \ZZ$.

Note that the calculation above also shows that $\ker \phi_1 = 0$ as $\phi_1$
is essentially given by multiplication by $2$.
Hence, looking at the exact sequence
\[
0 \cong
H_2(K_1) \oplus H_1(K_2) \xrightarrow{\rho_2}
H_2(K) \xrightarrow{\partial_2}
H_1(K_1 \cap K_2) \xrightarrow{\phi_1}
H_1(K_1) \oplus H_1(K_2),
\]
we see that $0 = \ker \phi_1 = \im \partial_2$, so
$H_2(K) = \ker \partial_2 = \im \rho_2 = 0$.

Moreover, as $K$ is a $2$-dimensional simplicial complex, we have
$H_n(K) \cong 0$ for all $n > 2$.

All in all, we obtain
\[
H_n(K) \cong
\begin{cases}
\ZZ & n = 0 \\
\ZZ / 2 \ZZ & n = 1 \\
0 & \text{otherwise}
\end{cases}.
\]

\section{The Klein bottle}

In this second section, we will describe another simplicial complex $K$
whose realization is homeomorphic to the Klein bottle, and compute its
homology using the (simplicial) excision theorem and the long exact
sequence for a couple.

This section is somewhat more sketchy than the first one.
As before, there may still be some sign or labeling mistakes left in the text.

\subsection*{The complex and the decomposition}

Here is a possible way to realize the Klein bottle as a simplicial complex:

\[
K :
\begin{tikzpicture}[baseline=-1.4cm, node distance=0.9cm]
\coordinate[label=above left:$a$] (a1) {};
\coordinate[below=of a1,label=left:$b$] (b1) {};
\coordinate[below=of b1,label=left:$c$] (c1) {};
\coordinate[below=of c1,label=below left:$a$] (a2) {};
\coordinate[right=of a2,label=below:$d$] (d1) {};
\coordinate[right=of d1,label=below:$e$] (e1) {};
\coordinate[right=of e1,label=below right:$a$] (a3) {};
\coordinate[above=of a3,label=right:$c$] (c2) {};
\coordinate[above=of c2,label=right:$b$] (b2) {};
\coordinate[above=of b2,label=above right:$a$] (a4) {};
\coordinate[left=of a4,label=above:$d$] (d2) {};
\coordinate[left=of d2,label=above:$e$] (e2) {};
\coordinate[below=of e2,label=below right:$f$] (f) {};
\coordinate[below=of f,label=below right:$g$] (g) {};
\coordinate[right=of g,label=above left:$h$] (h) {};
\coordinate[above=of h,label=above left:$i$] (i) {};
\draw (a1) -- (b1);
\draw (b1) -- (c1);
\draw (c1) -- (a2);
\draw (a2) -- (d1);
\draw (d1) -- (e1);
\draw (e1) -- (a3);
\draw (a3) -- (c2);
\draw (c2) -- (b2);
\draw (b2) -- (a4);
\draw (a4) -- (d2);
\draw (d2) -- (e2);
\draw (e2) -- (a1);
\draw (e2) -- (f);
\draw (f) -- (g);
\draw (g) -- (d1);
\draw (d2) -- (i);
\draw (i) -- (h);
\draw (h) -- (e1);
\draw (b1) -- (f);
\draw (f) -- (i);
\draw (i) -- (b2);
\draw (c1) -- (g);
\draw (g) -- (h);
\draw (h) -- (b2);
\draw (b1) -- (e2);
\draw (c1) -- (f);
\draw (a2) -- (g);
\draw (f) -- (d2);
\draw (g) -- (i);
\draw (d1) -- (h);
\draw (e1) -- (c2);
\draw (h) -- (c2);
\draw (i) -- (a4);
\end{tikzpicture}.
\]

The idea of our computation is finding a decomposition $K = K_1 \cup K_2$
into subcomplexes such that we can understand $H_\bullet(K_2)$ and
$H_\bullet(K, K_2)$ (where the latter can be identified with
$H_\bullet(K_1, K_1 \cap K_2)$ via excision) well and deduce what
$H_\bullet(K)$ must be by analyzing the long exact sequence
for the couple $(K, K_2)$.

To this end, we let
\[
K_1 \coloneqq
\begin{tikzpicture}[baseline=-1.4cm,node distance=0.9cm]
\coordinate (f2) {};
\coordinate[right=of f2] (e2) {};
\coordinate[below=of f2,label=left:$f$] (f) {};
\coordinate[below=of f,label=left:$g$] (g) {};
\coordinate[right=of g,label=right:$h$] (h) {};
\coordinate[above=of h,label=right:$i$] (i) {};
\coordinate[below=of g] (e1) {};
\coordinate[below=of h] (f1) {};
\draw (f) -- (g);
\draw (i) -- (h);
\draw (f) -- (i);
\draw (g) -- (h);
\draw (g) -- (i);
\end{tikzpicture}
\text{\quad and\quad}
K_2 \coloneqq
\begin{tikzpicture}[baseline=-1.9cm, node distance=1.2cm]
\coordinate[label=above left:$a$] (a1) {};
\coordinate[below=of a1,label=left:$b$] (b1) {};
\coordinate[below=of b1,label=left:$c$] (c1) {};
\coordinate[below=of c1,label=below left:$a$] (a2) {};
\coordinate[right=of a2,label=below:$d$] (d1) {};
\coordinate[right=of d1,label=below:$e$] (e1) {};
\coordinate[right=of e1,label=below right:$a$] (a3) {};
\coordinate[above=of a3,label=right:$c$] (c2) {};
\coordinate[above=of c2,label=right:$b$] (b2) {};
\coordinate[above=of b2,label=above right:$a$] (a4) {};
\coordinate[left=of a4,label=above:$d$] (d2) {};
\coordinate[left=of d2,label=above:$e$] (e2) {};
\coordinate[below=of e2,label=below right:$f$] (f) {};
\coordinate[below=of f,label=above right:$g$] (g) {};
\coordinate[right=of g,label=above left:$h$] (h) {};
\coordinate[above=of h,label=below left:$i$] (i) {};
\draw (a1) -- (b1);
\draw (b1) -- (c1);
\draw (c1) -- (a2);
\draw (a2) -- (d1);
\draw (d1) -- (e1);
\draw (e1) -- (a3);
\draw (a3) -- (c2);
\draw (c2) -- (b2);
\draw (b2) -- (a4);
\draw (a4) -- (d2);
\draw (d2) -- (e2);
\draw (e2) -- (a1);
\draw (e2) -- (f);
\draw (f) -- (g);
\draw (g) -- (d1);
\draw (d2) -- (i);
\draw (i) -- (h);
\draw (h) -- (e1);
\draw (b1) -- (f);
\draw (f) -- (i);
\draw (i) -- (b2);
\draw (c1) -- (g);
\draw (g) -- (h);
\draw (h) -- (b2);
\draw (b1) -- (e2);
\draw (c1) -- (f);
\draw (a2) -- (g);
\draw (f) -- (d2);
\draw (d1) -- (h);
\draw (e1) -- (c2);
\draw (h) -- (c2);
\draw (i) -- (a4);
\end{tikzpicture}.
\]
Note that we have
\[
K_1 \cap K_2 =
\begin{tikzpicture}[baseline=-1.6cm,node distance=1cm]
\coordinate (f2) {};
\coordinate[right=of f2] (e2) {};
\coordinate[below=of f2,label=left:$f$] (f) {};
\coordinate[below=of f,label=left:$g$] (g) {};
\coordinate[right=of g,label=right:$h$] (h) {};
\coordinate[above=of h,label=right:$i$] (i) {};
\coordinate[below=of g] (e1) {};
\coordinate[below=of h] (f1) {};
\draw (f) -- (g);
\draw (i) -- (h);
\draw (f) -- (i);
\draw (g) -- (h);
\end{tikzpicture}.
\]

\subsection*{Homology of the subcomplex}

Intuitively speaking, ``$K_2$ is just a thickened version of two
loops $\{a, b\} \cup \{a, c\} \cup \{a, c\}$ and
$\{a, e\} \cup \{d, e\} \cup \{a, d\}$ joined at $a$'', so we can expect
to have $H_1(K_2) \cong \ZZ$, $H_1(K_2) \cong \ZZ^2$ and $H_n(K_2) \cong 0$
for $n \notin \{0, 1\}$.

To be more precise, we can use a Mayer--Vietoris sequence to compute
$H_\bullet(K_2)$. We start with a decomposition
\[
K_2 =
\substack{
\left(
\begin{tikzpicture}[baseline=-1.4cm, node distance=1cm]
\coordinate[label=above left:$a$] (a1) {};
\coordinate[below=of a1,label=below left:$b$] (b1) {};
\coordinate[below=of b1,label=above left:$c$] (c1) {};
\coordinate[below=of c1,label=below left:$a$] (a2) {};
\coordinate[right=of a2,label=below:$d$] (d1) {};
\coordinate[right=of d1,label=below:$e$] (e1) {};
\coordinate[right=of e1,label=below right:$a$] (a3) {};
\coordinate[above=of a3,label=above right:$c$] (c2) {};
\coordinate[above=of c2,label=below right:$b$] (b2) {};
\coordinate[above=of b2,label=above right:$a$] (a4) {};
\coordinate[left=of a4,label=above:$d$] (d2) {};
\coordinate[left=of d2,label=above:$e$] (e2) {};
\coordinate[below=of e2,label=below:$f$] (f) {};
\coordinate[below=of f,label=above:$g$] (g) {};
\coordinate[right=of g,label=above:$h$] (h) {};
\coordinate[above=of h,label=below:$i$] (i) {};
\draw (a1) -- (b1);
\draw (c1) -- (a2);
\draw (a2) -- (d1);
\draw (d1) -- (e1);
\draw (e1) -- (a3);
\draw (a3) -- (c2);
\draw (b2) -- (a4);
\draw (a4) -- (d2);
\draw (d2) -- (e2);
\draw (e2) -- (a1);
\draw (e2) -- (f);
\draw (g) -- (d1);
\draw (d2) -- (i);
\draw (h) -- (e1);
\draw (b1) -- (f);
\draw (f) -- (i);
\draw (i) -- (b2);
\draw (c1) -- (g);
\draw (g) -- (h);
\draw (b1) -- (e2);
\draw (a2) -- (g);
\draw (f) -- (d2);
\draw (d1) -- (h);
\draw (e1) -- (c2);
\draw (h) -- (c2);
\draw (i) -- (a4);
\end{tikzpicture}
\right) \\
K_{2, 1}
}
\cup
\substack{
\left(
\begin{tikzpicture}[baseline=-1.4cm, node distance=1cm]
\coordinate[] (a1) {};
\coordinate[below=of a1,label=left:$b$] (b1) {};
\coordinate[below=of b1,label=left:$c$] (c1) {};
\coordinate[below=of c1] (a2) {};
\coordinate[right=of a2] (d1) {};
\coordinate[right=of d1] (e1) {};
\coordinate[right=of e1] (a3) {};
\coordinate[above=of a3,label=right:$c$] (c2) {};
\coordinate[above=of c2,label=right:$b$] (b2) {};
\coordinate[above=of b2] (a4) {};
\coordinate[left=of a4] (d2) {};
\coordinate[left=of d2] (e2) {};
\coordinate[below=of e2,label=right:$f$] (f) {};
\coordinate[below=of f,label=right:$g$] (g) {};
\coordinate[right=of g,label=left:$h$] (h) {};
\coordinate[above=of h,label=left:$i$] (i) {};
\draw (b1) -- (c1);
\draw (c2) -- (b2);
\draw (f) -- (g);
\draw (i) -- (h);
\draw (b1) -- (f);
\draw (i) -- (b2);
\draw (c1) -- (g);
\draw (h) -- (b2);
\draw (c1) -- (f);
\draw (h) -- (c2);
\end{tikzpicture}
\right) \\
K_{2, 2}
}.
\]

Note that
\[
K_{2, 1} =
\begin{tikzpicture}[baseline=-1cm, node distance=1cm]
\coordinate[label=above left:$c$] (c1) {};
\coordinate[below=of c1,label=left:$a$] (a2) {};
\coordinate[below=of a2,label=below left:$b$] (b2) {};
\coordinate[right=of b2,label=below:$i$] (i) {};
\coordinate[right=of i,label=below:$f$] (f) {};
\coordinate[right=of f,label=below right:$b$] (b1) {};
\coordinate[above=of b1,label=right:$a$] (a1) {};
\coordinate[above=of a1,label=above right:$c$] (c2) {};
\coordinate[left=of c2,label=above:$h$] (h) {};
\coordinate[left=of h,label=above:$g$] (g) {};
\coordinate[left=of a1,label=above left:$e$] (e) {};
\coordinate[left=of e,label=above left:$d$] (d) {};
\draw (a1) -- (b1);
\draw (c1) -- (a2);
\draw (a2) -- (d);
\draw (d) -- (e);
\draw (e) -- (a1);
\draw (a1) -- (c2);
\draw (b2) -- (a2);
\draw (a2) -- (d);
\draw (d) -- (e);
\draw (e) -- (a1);
\draw (e) -- (f);
\draw (g) -- (d);
\draw (d) -- (i);
\draw (h) -- (e);
\draw (b1) -- (f);
\draw (f) -- (i);
\draw (i) -- (b2);
\draw (c1) -- (g);
\draw (g) -- (h);
\draw (b1) -- (e);
\draw (a2) -- (g);
\draw (f) -- (d);
\draw (d) -- (h);
\draw (e) -- (c2);
\draw (h) -- (c2);
\draw (i) -- (a2);
\end{tikzpicture}
\]
realizes to a cylinder. We will just use that
\[
H_n(K_{2, 1}) \cong
\begin{cases}
\ZZ & n \in \{0, 1\} \\
0 & \text{otherwise}
\end{cases}
\]
and that $[c]$ \resp $\gamma_1 \coloneqq [\{a, d\} + \{d, e\} - \{a, e\}]$ is
a generator of $H_0(K_{2,1})$ \resp $H_1(K_{2, 1})$ without calculating these
groups explicitly.

Moreover,
\[
K_{2, 2} =
\begin{tikzpicture}[baseline=-0.7cm, node distance=1cm]
\coordinate[label=left:$i$] (i) {};
\coordinate[below=of i,label=left:$h$] (h) {};
\coordinate[right=of h,label=below:$c$] (c) {};
\coordinate[right=of c,label=right:$g$] (g) {};
\coordinate[above=of g,label=right:$f$] (f) {};
\coordinate[left=of f,label=above:$b$] (b) {};
\draw (b) -- (c);
\draw (f) -- (g);
\draw (i) -- (h);
\draw (b) -- (f);
\draw (i) -- (b);
\draw (c) -- (g);
\draw (h) -- (b);
\draw (c) -- (f);
\draw (h) -- (c);
\end{tikzpicture}
\]
is acyclic (as it can be built from acyclic complexes with acyclic
intersection at each step). Hence
\[
H_n(K_{2, 1}) \cong
\begin{cases}
\ZZ \cdot [c] \cong \ZZ & n = 0 \\
0 & \text{otherwise}
\end{cases}.
\]

The intersection
\[
K_{2, 1} \cap K_{2, 2} =
\begin{tikzpicture}[baseline=-0.7cm, node distance=1cm]
\coordinate[label=left:$i$] (i) {};
\coordinate[below=of i,label=left:$h$] (h) {};
\coordinate[right=of h,label=below:$c$] (c) {};
\coordinate[right=of c,label=right:$g$] (g) {};
\coordinate[above=of g,label=right:$f$] (f) {};
\coordinate[left=of f,label=above:$b$] (b) {};
\draw (b) -- (f);
\draw (i) -- (b);
\draw (c) -- (g);
\draw (h) -- (c);
\end{tikzpicture}
\]
is a disjoint union of two acyclic complexes, so we have
\[
H_n(K_{2, 1}) \cong
\begin{cases}
\ZZ \cdot [b] \oplus \ZZ \cdot [c] \cong \ZZ^2 & n = 0 \\
0 & \text{otherwise}
\end{cases}.
\]

For $n > 1$, the Mayer--Vietoris sequence yields an exact sequence
\[
0 \cong H_n(K_{2,1}) \oplus H_n(K_{2,2}) \to H_n(K_2) \to
H_{n-1}(K_{2,1} \cap K_{2,1}) \cong 0,
\]
so we indeed have $H_n(K_2) \cong 0$ for $n > 1$.
Moreover, $H_0(K_2) = \ZZ \cdot [c] \cong \ZZ$ since $K_2$ is connected.

Hence, the Mayer--Vietoris sequence for $n \leq 1$ looks like:
\[
\begin{tikzcd}[row sep = 0cm, column sep = small]
0 \ar{r} & \ZZ \cdot \gamma_1 \ar{r} & H_1(K_2) \ar[r, "\partial_1"] &
\ZZ \cdot [b] \oplus \ZZ \cdot [c] \ar[r, "\phi_1"] &
\ZZ[c_{K_{2,1}}] \oplus \ZZ[c_{K_{2,1}}] \ar{r} &
\ZZ \cdot [c] \ar{r} & 0 \\
& & & {[b]} \ar[r, mapsto] & ([c_{K_{2,1}}], -[c_{K_{2,2}}]) \\
& & & {[c]} \ar[r, mapsto] & ([c_{K_{2,1}}], -[c_{K_{2,2}}])
\end{tikzcd}
\]
where $c_{K_{2, k}}$ for $k \in \{1, 2\}$ denotes the vertex $c$ regarded
as a $0$-simplex of $K_{2, k}$. This yields an exact sequence
\[
0 \to \ZZ \cdot \gamma_1 \to H_1(K_2) \xrightarrow{\partial_1} \ker \phi_1 =
\ZZ \cdot [c - d] \to 0.
\]

Now, unwinding the definition of the boundary map, one can check that
$\gamma_2 \coloneqq [\{a, b\} + \{b, c\} - \{a, c\}] \in H_1(K_2)$ is a
preimage of $[c - d] \in H_0(K_{2, 1} \cap K_{2,2})$ under
$\partial_1$.\footnote{Intuitively, $\gamma_1$ is chosen to be the sum of two
$1$-chains which connect $b$ and $c$ in $K_{2,1}$ \resp $K_{2,2}$.}
Thus, sending $[c - b]$ to $\gamma_2$ yields a section of $\partial_1$ and
hence an isomorphism $H_1(K_2) \cong \ZZ \cdot \gamma_1 \oplus \ZZ \cdot
\gamma_2 \cong \ZZ^2$.\footnote{In fact, every short exact sequence
of abelian groups that is of the form $0 \to \ZZ \to A \to \ZZ \to 0$
splits for algebraic reasons and thus the middle term is always isomorphic to
$\ZZ^2$.}

\subsection*{Relative homology}
By excision, we have $H_\bullet(K, K_2) \cong H_\bullet(K_1, K_1 \cap K_2)$.
Now, $C_\bullet(K_1, K_1 \cap K_2)$ is rather simple:
\[
\begin{tikzcd}[row sep = 0cm]
\ldots & 3 & 2 & 1 & 0 \\
\ldots \ar{r} & 0 \ar{r} &
\ZZ \cdot [\{f, g, i\}] \oplus \ZZ \cdot [\{g, h, i\}] \ar[r, "d_1"] &
\ZZ \cdot [\{g, i\}] \ar{r} & 0 \\
& & {[\{f, g, i\}]} \ar[r, mapsto] & {[\{g, i\}]} \\
& & {[\{g, h, i\}]} \ar[r, mapsto] & {-[\{g, i\}]}
\end{tikzcd}
\]
where $[-]$ denotes equivalence classes in
$C_k(K_1, K_1 \cap K_2) = C_k(K_1) / C_k(K_1 \cap K_2)$.

Hence we have
\[
H_n(K_1, K_1 \cap K_2) \cong
\begin{cases}
\operatorname{coker} d_1 \cong \ZZ \cdot [\{g, i\}] / \ZZ[\{g, i\}] \cong 0
  & n = 1\\
\ker d_1 \cong \ZZ \cdot [\{f, g, i\} + \{g, h, i\}] \cong \ZZ & n = 2\\
0 & \text{otherwise}
\end{cases}.
\]
Let $\alpha \coloneqq [\{f, g, i\} + \{g, h, i\}]$ for future reference.

\subsection*{Long exact sequence of the couple}

Note that $H_0(K) \cong \ZZ$ as $K$ is connected.
Moreover, we know that $H_n(K) \cong 0$ for $n > 2$ as $K$ is a
$2$-dimensional complex.
For $n \in \{1, 2\}$ we consider the corresponding part of the
long exact sequence of the pair $(K, K_2)$:
\newcommand*{\isom}[1]{\arrow[#1,"\rotatebox{90}{\(\cong\)}", phantom]}
\[
\begin{tikzcd}[row sep = 0.1cm, column sep = small]
H_2(K_2) \ar{r} \isom{d} & H_2(K) \ar{r} \isom{d} &
H_2(K, K_2) \ar{r} \isom{d} &
H_1(K_2) \ar{r} \isom{d} & H_1(K) \ar{r} \isom{d} & H_1(K, K_2) \isom{d} \\
H_2(K_2) \ar{r} \isom{d} & H_2(K) \ar{r} \isom{d} &
H_2(K_1, K_1 \cap K_2) \ar{r} \isom{d} &
H_1(K_2) \ar{r} \isom{d} & H_1(K) \ar{r} \isom{d} &
H_1(K_1, K_1 \cap K_2) \isom{d} \\
0 \ar[r, swap, "\iota_2"] & H_2(K) \ar[r, swap, "\rho_2"] &
\ZZ \cdot \alpha \ar[r, swap, "\partial_2"] &
\ZZ \cdot \gamma_1 \oplus \ZZ \cdot \gamma_2 \ar{r} & H_1(K) \ar{r} & 0
\end{tikzcd}
\]

Unwinding definitions, one sees that
$\partial_2(\alpha) =[\{f, g\} + \{g, h\} + \{h, i\} - \{f, i\}]$ which
corresponds to ``going around the inner square once''.
This cycle is equivalent to ``going around the outer square'' in
$K_2$,\footnote{More precisely, one can assign signs to $2$-simplices of $K_2$
in a way what the cycle given by the sum of these has
$(\{f, g\} + \{g, h\} + \{h, i\} - \{f, i\} -
(\{a, b\} + \{b, c\} - \{a, c\} + \{a, d\} + \{d, e\} - \{a, e\} +
\{a, c\} - \{b, c\} - \{a, b\} + \{a, d\} + \{d, e\} - \{a, e\})$
as its boundary, but we refrain from doing the long calculation here.} \ie
\begin{align*}
\partial_2(\alpha) &
= [\{a, b\} + \{b, c\} - \{a, c\} + \{a, d\} + \{d, e\} - \{a, e\} + \\
& \qquad \{a, c\} - \{b, c\} - \{a, b\} + \{a, d\} + \{d, e\} - \{a, e\}] \\
& = 2 \cdot [\{a, d\} + \{d, e\} - \{a, e\}] = 2 \cdot \gamma_1.
\end{align*}

Hence $\partial_2$ corresponds to the map $\ZZ \to \ZZ^2$ which sends
$k$ to $(2k, 0)$.
This yields
\[
H_1(K) \cong \operatorname{coker}(\partial_1) =
\faktor{(\ZZ \cdot \gamma_1 \oplus \ZZ \cdot \gamma_2)}{
\ZZ \cdot (2 \gamma_1, 0)} \cong \ZZ/2\ZZ \oplus \ZZ.
\]
Moreover, we have $0 = \ker \partial_1 = \im \rho_2$. Hence
$\ker \rho_2 = H_2(K)$, but $\ker \rho_2 = \im \iota_2 = 0$, so
$H_2(K) \cong 0$.

All in all, we have:
\[
H_n(K) \cong
\begin{cases}
\ZZ & n = 0 \\
\ZZ/2\ZZ \oplus \ZZ & n = 1 \\
0 & \text{otherwise}
\end{cases}.
\]

\end{document}
