% A presentation about Riehl & Shulman's type theory for infinity-categories
% by Aras Ergus.

% This work is licensed under a Creative Commons Attribution-ShareAlike 4.0
% International License.
% See the following website for details:
% https://creativecommons.org/licenses/by-sa/4.0/

% This file is based on the following template by Till Tantau:
% http://mirrors.ctan.org/macros/latex/contrib/beamer/doc/solutions/generic-talks/generic-ornate-15min-45min.en.tex

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\documentclass[aspectratio=54]{beamer}

\mode<presentation>
{
  % \usetheme{Hannover} % With a sidebar.
  \usetheme{Singapore} % Without a sidebar.

  \setbeamercovered{transparent}

  % Display alerts as bold text (and not in red).
  \setbeamercolor{alerted text}{fg=}
  \setbeamerfont{alerted text}{series=\bfseries}
}

\usepackage[english]{babel}
\usepackage{microtype}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{csquotes}
\usepackage[
  type={CC},
  modifier={by-sa},
  version={4.0},
]{doclicense}

\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{bbm}
\usepackage{mathabx}
\usepackage{stmaryrd}
\usepackage{mathpartir}
\usepackage{mathtools}
\usepackage{tikz-cd}
\usepackage{extarrows}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Math macros

% "Operators".
\DeclareMathOperator{\Id}{Id}
\DeclareMathOperator{\Hom}{Hom}
\DeclareMathOperator{\Map}{Map}
\DeclareMathOperator{\Fun}{Fun}
\DeclareMathOperator{\Nat}{Nat}

% Numbers.
\newcommand{\NN}{\mathbb{N}}

% Categories.
\newcommand{\spaces}{\mathcal{S}}

\newtheorem*{proposition}{Proposition}
\newtheorem*{remark}{Remark}

\theoremstyle{definition}
\newtheorem*{convention}{Convention}
\newtheorem*{notation}{Notation}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Presentation preamble

\title{A type theory for synthetic $\infty$-categories}
\subtitle{
  after a homonymous paper by E.\ Riehl and M.\ Shulman
}
\author{Aras Ergus}
\date{July 1, 2020}

% If you wish to uncover everything in a step-wise fashion, uncomment
% the following command:

% \beamerdefaultoverlayspecification{<+->}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% And now the actual presentation!

\begin{document}

\begin{frame}
  \titlepage
  \tiny{\doclicenseThis}
\end{frame}

\begin{frame}{Outline}
  \tableofcontents
\end{frame}

\section{Motivation}

\begin{frame}{Why should homotopy theorists care about homotopy type theory?}
  HoTT provides a \enquote{synthetic} framework in which one can only talk about
  \enquote{homotopically correct} statements and constructions.
  \pause

  \vfill

  For example, in this world,\pause
  \begin{itemize}
    \item all maps one can write down are continuous (if between
      spaces) / functorial (if between categories),\pause
    \item two functions are \enquote{equal} if they are homotopic,\pause
    \item in particular, one cannot distinguish between (homotopy) equivalent
      objects,\pause
    \item \enquote{unique} means that the space of choices is contractible.
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{What does this buy us?}
  This alternative language is arguably simpler and more intuitive.
  \pause

  \vfill

  For example, one can prove the following version of the Yoneda lemma
  which (with a little bit of sloppy notation) looks a lot like the
  classical one:
  \pause

  \begin{theorem}[Theorem 9.1]
    Let $\mathcal{A}$ be an $\infty$-category, $\mathcal{C} \to \mathcal{A}$ a
    covariant family and $a \in \mathcal{A}$.
    Then the following is an equivalence:
    \[
      \begin{tikzcd}[row sep=tiny]
        \mathcal{C}_a \ar[r, leftrightarrow] &
          \Nat(\Hom(a, -), \mathcal{C}_{(-)}) \\
        u \ar[r, mapsto] & (f \mapsto \mathcal{C}_f(u)) \\
        \varphi(\Id_a) & \ar[l, mapsto] \varphi \\
      \end{tikzcd}
    \]
  \end{theorem}
\end{frame}

\begin{frame}{What's the catch?}
  \begin{itemize}
    \item In any given such framework, that are things we cannot express
      in it which are nevertheless interesting in concrete models.\pause
    \item One has to know how to \enquote{interpet the synthetic language
      in the real world}.
      Making this translation work is rather tedious.
  \end{itemize}
\end{frame}

\section[Why CSS?]{Why a synthetic theory of complete Segal spaces?}

\begin{frame}{The model we will mimic}
  While trying to axiomatize the theory of $\infty$-categories, we will draw
  inspiration from (complete) Segal spaces.

  \vfill

  There are several reasons for this choice.
\end{frame}

\begin{frame}{A \enquote{type-theoretic} reason}
  Reedy fibrant simplicial spaces is a model of \enquote{ordinary}
  homotopy type theory, so we can try to augment this model to
  be able to talk about (complete) Segal spaces.
  \pause

  \vfill

  \textbf{N.B.} We cannot extend the classical simplicial model of HoTT
  to quasicategories because types in this model corresponds to Kan complexes,
  so not every $\infty$-category would have an \enquote{underlying type}.
\end{frame}

\begin{frame}{First \enquote{homotopy-theoretic} reason}
  The Segal condition for Reedy fibrant simplicial spaces can be expressed
  in terms of a single equivalence of simplicial spaces:
  \pause

  \begin{definition}
    The \emph{horizontal embedding} of a simplicial set $\mathbb{X}$ is the
    simplicial space $X$ is given by $X_{k, l} \coloneqq \mathbb{X}_k$
    for all $k, l \in \NN$.

    For $n \in \NN$ and $i \in \{0, \ldots, n\}$, let $F(n)$ be the
    horizontal embedding of $\Delta^n$ and $L(n, i)$
    the horizontal embedding of $\Lambda^n_i$.
  \end{definition}
  \pause

  \begin{theorem}[Theorem A.21]
    A Reedy fibrant simplicial space $X$ is a Segal space if and only if
    \[
      \Map(F(2), X) \to \Map(L(2, 1), X)
    \]
    is a trivial Reedy fibration.
  \end{theorem}
\end{frame}

\begin{frame}{Another \enquote{homotopy-theoretic} reason}
  Similarly, completeness for Segal spaces can be expressed
  in terms of a single equivalence of simplicial spaces:
  \pause

  \begin{definition}
    Let $E(1)$ be the horizontal embedding of the nerve of the
    \enquote{walking isomorphism} (i.\,e.\ the category with two
    distinct objects and a unique isomorphism between them).
  \end{definition}
  \pause

  \begin{theorem}[Theorem A.25]
    A Segal space $X$ is complete if and only if
    \[
      \Map(E(1), X) \to \Map(F(0), X) \cong X
    \]
    is a trivial Reedy fibration.
  \end{theorem}
\end{frame}


\section[Interlude]{}

\begin{frame}{Bear with me please}
  We will need to know a bit about the syntax and semantics
  of type theory to understand/appreciate why the synthetic theory
  works the way it does.
  \pause

  \vfill

  However, we won't state all the rules needed to make our type theory work
  (and be sloppy while dealing with rules we do state), and
  gloss over many details while describing how type-theoretic and
  homotopy-/category-theoretic notions relate to each other.
\end{frame}

\section[Syntax]{Syntax of type theory}

\begin{frame}{Judgements}
  \begin{definition}
    A \emph{judgement $\varphi$} is, morally speaking, a statement we make in
    our theory.
  \end{definition}
  \pause

  \begin{example}
    \begin{itemize}
      \item $\bot$
      \item $s \leq t \wedge t \leq u$
      \item $1 = 0$
      \item $A\text{ type}$
      \item $(a, b) : A \times B$
      \item $p \equiv (\pi_1(p), \pi_2(p))$
      \item $\prod_{x : A} B\text{ type}$ (where $x$ can occur in $B$ as a
        variable)
    \end{itemize}
  \end{example}
\end{frame}

\begin{frame}{Contexts}
  \begin{definition}
    A \emph{context $\Gamma$} is a (\enquote{well-formed}) finite list of
    judgements.
    It can be thought of as the collection of assumptions we are
    currently working under.
    \pause

    We will work with expressions that look like
    \[
      \Gamma \vdash \varphi,
    \]
    which more or less means that
    \begin{center}
      \emph{the judgement $\varphi$ can be made under the assumption of the
      judgements in $\Gamma$}.
  \end{center}
  \end{definition}
\end{frame}

\begin{frame}{Inference rules}
  Inference rules are essentially the axioms which tell us when we can
  deduce statements of the form $\Gamma \vdash \varphi$.
  We depict such a rule as a list of premises separated from their conclusion
  by a vertical line.
  \pause

  \vfill

  \begin{example}
    \begin{mathpar}
      \inferrule{\ }{
        \Gamma \vdash \star : \mathbf{1}
      },

      \inferrule{\Gamma \vdash x \equiv y \\ \Gamma \vdash y \equiv z}{
        \Gamma \vdash x \equiv z
      },

      \inferrule{\Gamma \vdash \varphi}{\Gamma \vdash \varphi \vee \psi},

      \inferrule{\Gamma \vdash A\text{ type} \\ \Gamma \vdash a : \mathbf{0}}{
        \Gamma \vdash \operatorname{ind}_{\mathbf{0}}(A, a) : A
      },

      \inferrule{
        \Gamma \vdash A\text{ type} \\ \Gamma, x : A \vdash B\text{ type}
      }{
        \Gamma \vdash \prod_{x : A} B\text{ type}
      }.
    \end{mathpar}
  \end{example}
  \pause

  \vfill

  \textbf{N.B.} The last example demonstrates that we do need the extra
  inference layer to write down all axioms -- we couldn't introduce and then
  bind variables with just $\vdash$.
\end{frame}

\begin{frame}{(Dependent) type theories}
  \begin{definition}
    A \emph{logical system} consists of a grammar describing well-formed
    judgements and a collection of inference rules.
    \pause

    \emph{Type theory} is a blurry term for a logical system that involves
    \enquote{type-level} judgements like $A\text{ type}$,
    \enquote{inhabitance} judgements like $a : A$ and
    \enquote{term-level} judgements like $x \equiv y$.
    \pause

    A type theory is called \emph{dependent} if its types are allowed to depend
    on terms of other types.\footnote{A typical case when a type depends on
    terms of other types is when one has an \enquote{equality type} $x =_A y$
    for $x, y : A$.}
  \end{definition}
  \pause

  \vfill

  In fact, our \enquote{type theory} for $\infty$-categories will have two
  additional layers dealing with the combinatorics of simplices, their
  boundaries, horns etc.
\end{frame}

\section[Semantics]{Semantics of type theory}

\begin{frame}{Models}
  \begin{definition}
    A \emph{model} of a logical system is a concrete mathematical object
    in which its judgements correspond to concrete mathematical statements.
  \end{definition}
  \pause

  \vfill

  Usually, one agrees on a common form for those \enquote{models}.\pause

  \vfill

  \begin{example}
     There is a logical system for group theory, with judgements like
     $\forall g \ \forall h \ g \cdot h \cdot g^{-1} \cdot h = \mathbf{e}$
     and inference rules describing group axioms.

     One can interpret $\cdot$, $\mathbf{e}$ and $(-)^{-1}$ in any group,
     so it's reasonable to say that every concrete group is a model
     for this logical system.
  \end{example}
\end{frame}

\begin{frame}{Basic categorical semantics for type theories}
  There is an evident similarity between type-theoretic and category-theoretic
  constructions which you may have seen quite a few times by now:

  \begin{center}\begin{tabular}{| c | c |}
    \hline
    \textbf{Category theory} &
    \textbf{Type theory} \\ \hline \hline
    object & type \\ \hline
    morphism & term of a function type \\ \hline
    initial/terminal object & $\mathbf{0}$/$\mathbf{1}$ \\ \hline
    categorical product & product type \\ \hline
    \ldots & \ldots \\ \hline
  \end{tabular}\end{center}
  \pause

  Hence we should be able to interpret certain categories as
  a \enquote{category of types} modeling our type theory.
\end{frame}

\begin{frame}{Incorporating dependence into the semantics}
  Given a type $A$, there are (in general) types $B$ which only exist
  under the assumption $x : A$ (i.\,e.\ $x : A \vdash B\text{ type}$).
  Moreover, we could iterate this process, i.\,e.\ consider
  types that exist in the context $x : A, y : B$; or more generally
  have a \enquote{category of types} for every context $\Gamma$.
  \pause

  \vfill

  In order to reflect this in our models, we will enhance our \enquote{category
  of types} in the following ways:\pause
  \begin{enumerate}
    \item We will work with a category $\mathcal{C}$ of \emph{contexts}, where
      morphisms are \enquote{inferences}.\pause
    \item We will have a Grothendieck fibration
      $\mathcal{T} \to \mathcal{C}$ where the fiber over
      $\Gamma \in \mathcal{C}$ corresponds to \enquote{the category of types
      that exist in the context $\Gamma$}.\pause
    \item We will have a functor $\mathcal{T} \to \mathcal{C}^{[1]}$ that
      corresponds to mapping $A$ over $\Gamma$ (meaning that
      $\Gamma \vdash A\text{ type}$) to $(\Gamma, x : A) \to \Gamma$.
  \end{enumerate}
\end{frame}

\begin{frame}{Dependent types in practice}
  Usually, the fiber $\mathcal{T}_C$ over $C \in \mathcal{C}$ is some sort of
  overcategory $\mathcal{C}_{/C}$, so we can extend our analogy as follows:
  \pause

  \begin{center}\begin{tabular}{| c | c |}
    \hline
    \textbf{Category theory} &
    \textbf{Type theory} \\ \hline \hline
    terminal object $\Asterisk$ & empty context \\ \hline
    $D \to C$ in $\mathcal{C}_{/C}$ & $c : C \vdash D_c\text{ type}$ \\ \hline
    composite $D \to C \to \Asterisk$ &
    $c : C \vdash D_c\text{ type} \ \leadsto \
    \vdash \sum_{c : C} D_c\text{ type}$ \\ \hline
    right adjoint to $C \times_{\Asterisk} (-)$ &
    $c : C \vdash D_c\text{ type} \ \leadsto \
    \vdash \prod_{c : C} D_c\text{ type}$ \\ \hline
    \ldots & \ldots \\ \hline
  \end{tabular}\end{center}
\end{frame}

\section[TT with shapes]{The type theory with shapes}

\begin{frame}{A problem}
  Recall that in order to be able to express the Segal condition and
  completeness for a simplicial space $X$, we need to work with
  simplicial spaces of the form $\Map(K, X)$ for horizontal embeddings $K$
  of certain \enquote{small} simplicial sets like $\Delta^n$ or $\Lambda^n_i$.
  \pause

  \vfill

  However, these horizontal embeddings are not (necessarily) Reedy fibrant, so
  we need to incorporate them into our synthetic theory separately.
\end{frame}

\begin{frame}{A solution}
  We will restrict our attention to subsimplicial sets of $(\Delta^1)^n$'s
  (which include in particular $\Delta^n$'s, $\partial \Delta^n$'s,
  $\Lambda^n_i$'s etc.).
  \pause

  \vfill

  \begin{definition}
    A \emph{cube} is a finite product of copies of $[1]$.
    We consider it as a partial order.
    \pause

    A \emph{tope $\phi$ (in $k$ variables)} is a proposition in $k$ variables
    constructed using $0$ and $1$ (where $0, 1 \in [1]$), $\equiv$, $\leq$,
    conjuctions and disjunctions.
    \pause

    A \emph{shape} is a subset of a cube $[1]^k$ of the
    form $\{(t_1, \ldots, t_k) \in I | \phi(t_1, \ldots, t_k)\}$
    where $\phi$ is a tope in $k$ variables.
  \end{definition}
  \pause

  \vfill

  \begin{example}
    \begin{itemize}
      \item $\Delta^n \cong
        \{(t_1, \ldots, t_n) \in [1]^n | t_1 \leq \ldots \leq t_n\}$
        can be realized as a shape.
        \pause
      \item $\Lambda^2_1$ can be realized as
        $\{(s, t) \in [1]^2 | s \equiv 1 \vee t \equiv 0\}$.
    \end{itemize}
  \end{example}
\end{frame}

\begin{frame}{Cubes and topes in type theory}
  We will introduce new syntax in our logical system to encode cubes and
  topes.\pause

  This will result in three different types of judgements and contexts:
  One for cubes (usually $\Xi$), one for topes (usually $\Phi$) and one for
  usual type theory with dependent types (usually $\Gamma$).\pause

  Some inference rules for the first two \enquote{layers} are:
  \begin{example}
    \begin{mathpar}
      \inferrule{I \text{ cube} \\ J\text{ cube}}{I \times J\text{ cube}},
      \inferrule{\ }{\Xi \vdash 0 : [1]},

      \inferrule{\Xi \vdash s : I \\ \Xi \vdash t : I}{
        \Xi \vdash s \equiv t\text{ tope}
      },
      \inferrule{\Xi | \Phi \vdash \phi \wedge \psi}{\Xi | \Phi \vdash \psi},
      \inferrule{\ }{x : [1] | \cdot \vdash x \leq 1}
    \end{mathpar}
  \end{example}
\end{frame}

\begin{frame}{Shapes in type theory}
  \begin{convention}
    \[\{t : I | \phi\}\text{ shape}\]
    will be a shorthand for
    \[
      I\text{ cube} \quad \text{and} \quad t : I \vdash \phi\text{ tope}.
    \]
  \end{convention}
\end{frame}

\begin{frame}[fragile]{Extension types}
  Next, we determine how shapes and types interact.
  \pause

  For every shape $\{t : I | \psi\}$ and every type $A$ we would like to have
  a type of functions $\{t : I | \psi\} \to A$.
  \pause

  For this, it is enough to construct a type of extensions
  \[
    \begin{tikzcd}
      \{t: I | \phi\} \ar[r] \ar[d, hookrightarrow] & A \\
      \{t: I | \psi\} \ar[ru, dashed] &
    \end{tikzcd}
  \]
  for $t : I | \phi \vdash \psi$.
  \pause

  Now, we can also make the target \enquote{vary in elements of
  $\{t: I | \phi\}$} -- in other words, instead of extensions of functions,
  we can more generally look for extensions of a section of a fibration.
\end{frame}

\begin{frame}{Some rules for extension types}
  \begin{mathpar}
    \inferrule{
      \{t : I | \phi\}\text{ shape} \\
      \{t : I | \psi\}\text{ shape} \\
      t:I | \phi \vdash \psi \\
      \Xi | \Phi \vdash \Gamma\text{ context} \\
      \Xi, t:I | \Phi, \psi | \Gamma \vdash A\text{ type} \\
      \Xi, t: I | \Phi, \phi | \Gamma \vdash a : A
    }{
      \Xi | \Phi | \Gamma \vdash
      \langle \prod_{t : I | \psi} A |^{\phi}_a \rangle
    }

    \inferrule{
      \{t : I | \phi\}\text{ shape} \\
      \{t : I | \psi\}\text{ shape} \\
      t:I | \phi \vdash \psi \\
      \Xi | \Phi \vdash \Gamma\text{ context} \\
      \Xi, t:I | \Phi, \psi | \Gamma \vdash A\text{ type} \\
      \Xi, t: I | \Phi, \phi | \Gamma \vdash a : A \\
      \Xi, t: I | \Phi, \psi | \Gamma \vdash b : A \\
      \Xi, t: I | \Phi, \phi | \Gamma \vdash b \equiv a
    }{
      \Xi | \Phi | \Gamma \vdash
      \lambda t^{I | \psi}.b : \langle \prod_{t : I | \psi} A |^{\phi}_a \rangle
    }
  \end{mathpar}
\end{frame}

\begin{frame}[fragile]{The final model}
  \[
    \begin{tikzcd}
      \text{$Y \twoheadrightarrow X \twoheadrightarrow T
      \hookrightarrow [1]^n$} \ar[r, dashed, no head]
      & \mathcal{T} \ar[rd] \ar[rr] &
      & (\mathcal{C}_2)^{[1]}_{\mathcal{C}_1} \ar[ld] \\
      \text{$X \twoheadrightarrow T \hookrightarrow [1]^n$}
      \ar[rr, dashed, no head] &
      & \mathcal{C}_2 \ar[d] \\
      \text{$T \hookrightarrow [1]^n$} \ar[rr, dashed, no head] &
      & \mathcal{C}_1 \ar[d] \\
      \text{$[1]^n$} \ar[rr, dashed, no head] &
      & \mathcal{C}_0 \\
    \end{tikzcd}
  \]
\end{frame}

\end{document}
