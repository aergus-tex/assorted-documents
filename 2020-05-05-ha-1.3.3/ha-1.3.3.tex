% A presentation about Section 1.3.3 of Lurie's Higher Algebra by Aras Ergus.

% This work is licensed under a Creative Commons Attribution-ShareAlike 4.0
% International License.
% See the following website for details:
% https://creativecommons.org/licenses/by-sa/4.0/

% This file is based on the following template by Till Tantau:
% http://mirrors.ctan.org/macros/latex/contrib/beamer/doc/solutions/generic-talks/generic-ornate-15min-45min.en.tex

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\documentclass{beamer}

\mode<presentation>
{
  % \usetheme{Hannover} % With a sidebar.
  \usetheme{Singapore} % Without a sidebar.

  \setbeamercovered{transparent}

  % Display alerts as bold text (and not in red).
  \setbeamercolor{alerted text}{fg=}
  \setbeamerfont{alerted text}{series=\bfseries}
}

\usepackage[english]{babel}
\usepackage{microtype}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{csquotes}
\usepackage[
  type={CC},
  modifier={by-sa},
  version={4.0},
]{doclicense}

\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{bbm}
\usepackage{mathabx}
\usepackage{stmaryrd}
\usepackage{mathtools}
\usepackage{tikz-cd}
\usepackage{extarrows}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Math macros

\usepackage{math-macros}

% Functions.
\DeclareMathOperator{\Id}{Id}
\DeclareMathOperator{\Pro}{Pro}

% Number "domain"s.
\newcommand{\NN}{\mathbb{N}}
\newcommand{\QQ}{\mathbb{Q}}
\newcommand{\ZZ}{\mathbb{Z}}

% Spectra.
\newcommand{\Sp}{\mathbf{Sp}}

\newtheorem*{proposition}{Proposition}
\newtheorem*{remark}{Remark}

\theoremstyle{definition}
\newtheorem*{convention}{Convention}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Presentation preamble

\title{The universal property of $\mathcal{D}^-(\mathcal{A})$}
\subtitle{based on Section 1.3.3 of Higher Algebra by Jacob Lurie}
\author{Aras Ergus}
\date{May 5, 2020}

% If you wish to uncover everything in a step-wise fashion, uncomment
% the following command:

% \beamerdefaultoverlayspecification{<+->}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% And now the actual presentation!

\begin{document}

\begin{frame}
  \titlepage
  \tiny{\doclicenseThis}
\end{frame}

\begin{frame}{Conventions}
  Contrary to the conventions of Higher Algebra, we will omit the nerve
  construction of $1$-categories from our notation.

  \ %

  We will often decorate functor categories with some symbols to mean
  a full subcategory spanned by functors satisfying some property.
  Examples:
  \begin{itemize}
    \item $\Fun^{rex}$: subcategory of right exact functors (i.\,e.\ those
      which preserve finite colimits).
    \item $\Fun^{|-|, \amalg}$: subcategory of functors
      which preserve geometric realizations and finite coproducts.
  \end{itemize}

  \ %

  We fix an abelian category $\mathcal{A}$ with enough projective objects.
  Changing universes if necessary, we assume that $\mathcal{A}$ is small.
\end{frame}

\begin{frame}{The main theorem}
  \begin{definition}[1.3.3.1]
    Let $\mathcal{C}$ and $\mathcal{C}'$ be stable $\infty$-categories with
    t-structures.

    A functor $F \colon \mathcal{C} \to \mathcal{C}'$ is called
    \emph{right t-exact (rtex)} if it is exact (i.\,e.\ preserves finite
    limits and colimits) and
    $F(\mathcal{C}_{\geq 0}) \subseteq \mathcal{C}'_{\geq 0}$.
  \end{definition}

  \begin{theorem}[1.3.3.2]
    Let $\mathcal{C}$ be a stable $\infty$-category equipped with a left
    complete t-structure.

    Then
    \begin{align*}
      \Fun^{
        \text{rtex},
        \mathcal{A}_\text{proj} \mapsto \mathcal{C}^\heartsuit
      }(\mathcal{D}^-(\mathcal{A}), \mathcal{C}) &
      \to \Fun^{\text{rex}_\text{ab}}(\mathcal{A}, \mathcal{C}^\heartsuit) \\
      F &
      \mapsto \tau_{\leq 0} \circ (F | \mathcal{D}^-(\mathcal{A})^\heartsuit)
    \end{align*}
    is an equivalence.
  \end{theorem}
\end{frame}

\begin{frame}[label=proof-diagram]{A proof sketch for the main theorem}
  \[
    \begin{tikzcd}[ampersand replacement=\&]
      \Fun^{
        \text{rtex},
        \mathcal{A}_\text{proj} \mapsto \mathcal{C}^\heartsuit
      }(\mathcal{D}^-(\mathcal{A}), \mathcal{C})
      \ar[
        d, leftrightarrow, swap,
        "\parbox{3.5em}{\tiny{
          \hyperlink{rtex-to-rex}{\beamergotobutton{1.3.3.11}} \\
          \hyperlink{t-on-d}{\beamergotobutton{1.3.3.16}}
         }}"
      ] \& \\
      \Fun^{
        |-|, \amalg, \mathcal{A}_\text{proj} \mapsto \mathcal{C}^\heartsuit
      }(\mathcal{D}^-_{\geq 0}(\mathcal{A}), \mathcal{C}_{\geq 0})
      \ar[
        r , leftrightarrow,
        "\text{
          \hyperlink{precise-characterization}{\beamergotobutton{1.3.3.8}}
        }"
      ] \&
      \Fun^{\amalg}(\mathcal{A}_\text{proj}, \mathcal{C}^\heartsuit)
      \ar[
        d, leftrightarrow,
        "\parbox{3.5em}{\tiny{1.3.3.9 \\ ``projective resolutions''}}"
      ] \\
      \& \Fun^{\text{rex}_\text{ab}}(\mathcal{A}, \mathcal{C}^\heartsuit)
    \end{tikzcd}
  \]
\end{frame}

\begin{frame}[label=realization-lemma]{A lemma about geometric realizations}
  \begin{lemma}[1.3.3.10]
    \begin{enumerate}
      \item If an $\infty$-category $\mathcal{C}$ admits finite coproducts and
        geometric realizations of simplicial objects, then it admits all
        finite colimits.
        The converse is true if $\mathcal{C}$ is an $n$-category for some
        $n \in \NN$.
      \item If  $F \colon \mathcal{C} \to \mathcal{D}$ is a functor between
        $\infty$-categories admitting finite coproducts and
        geometric realizations of simplicial objects that preserves
        finite coproducts and geometric realizations, then $F$ is
        right exact.
        The converse is true if $\mathcal{C}$ and $\mathcal{D}$ are
        $n$-categories for some $n \in \NN$.
    \end{enumerate}
  \end{lemma}
\end{frame}

\begin{frame}{Proof of the geometric realization lemma}
  \begin{proof}[Proof sketch]
    In order to construct all finite colimits from finite coproducts and
    geometric realizations, it is enough to construct coequalizers.

    Note there is a (non-full) inclusion
    $\iota \colon ([1] \rightrightarrows [0]) \hookrightarrow \Delta^\op$
    of the coequalizer shape into $\Delta^\op$.
    Moreover, using the pointwise formula for Kan extensions, one can
    show that $\iota_!$ exists because finite coproducts exist.

    Hence $\operatorname{colim}_{[1] \rightrightarrows [0]} \simeq
    \operatorname{colim}_{\Delta^\op} \circ \iota_!$ exists.

    \ %

    For the part about $n$-categories, one shows that
    $\operatorname{colim}_{\Delta^\op} \simeq
    \operatorname{colim}_{\Delta^\op_{\leq n}} \circ \iota^*_{\leq n}$
    using some connectivity estimates after applying a Yoneda embedding and
    notes that the latter is a finite colimit.
  \end{proof}
\end{frame}

\begin{frame}[label=rtex-to-rex]{%
  From right t-exact functors to right exact functors
}
  \begin{lemma}[1.3.3.11]
    Let $\mathcal{C}$, $\mathcal{C}'$ be stable $\infty$-categories equipped
    with $t$-structures.

    Then
    \begin{enumerate}
      \item If $\mathcal{C}$ is right bounded, then restriction along
        $\mathcal{C}_{\geq 0} \hookrightarrow \mathcal{C}$ induces an
        equivalence $\Fun^{\text{rtex}}(\mathcal{C}, \mathcal{C}') \simeq
        \Fun^{\text{rex}}(\mathcal{C}_{\geq 0}, \mathcal{C}'_{\geq 0})$.
      \item If $\mathcal{C}$ and $\mathcal{C}'$ are left complete,
        then $\mathcal{C}_{\geq 0}$ and $\mathcal{C}'_{\geq 0}$ admit
        geometric realizations of simplicial objects, and a functor
        $F \colon \mathcal{C}_{\geq 0} \to \mathcal{C}'_{\geq 0}$
        is right exact if and only if it preserves finite coproducts
        and geometric realizations.
    \end{enumerate}
  \end{lemma}
\end{frame}

\begin{frame}{Proof the ``rtex to rex'' lemma, part 1}
  \begin{proof}[Proof sketch for (1)]
    Since $\mathcal{C} = \bigcup_n \mathcal{C}_{\geq -n}$ by right boundedness,
    we have
    \[
      \Fun^{rtex}(\mathcal{C}, \mathcal{C}') \simeq
      \operatorname{lim}(
      \ldots \to
      \Fun^{rex}(\mathcal{C}_{\geq -1}, \mathcal{C}'_{\geq -1}) \to
      \Fun^{rex}(\mathcal{C}_{\geq 0}, \mathcal{C}'_{\geq 0})
      ).
    \]
    Now the tower on the RHS is essentially constant as
    $\Fun^{rex}(\mathcal{C}_{\geq -n-1}, \mathcal{C}'_{\geq -n-1}) \to
    \Fun^{rex}(\mathcal{C}_{\geq n}, \mathcal{C}'_{\geq n})$
    has a (homotopy) inverse given by ``conjugation by $\Sigma$''.
  \end{proof}
\end{frame}

\againframe<2>{rtex-to-rex}

\begin{frame}{Proof the ``rtex to rex'' lemma, part 2}
  \begin{proof}[Proof sketch for (2)]
    Left completeness means that
    \[
      \mathcal{C}_{\geq 0} \simeq \operatorname{lim}(\ldots \to
     (\mathcal{C}_{\geq 0})_{\leq 1} \to (\mathcal{C}_{\geq 0})_{\leq 0}).
    \]
    Moreover, each $(\mathcal{C}_{\geq 0})_{\leq n}$ is an finitely
    cocomplete $(n+1)$-category.
    Hence, by \hyperlink{realization-lemma}{\beamergotobutton{Lemma 1.3.3.10}}
    about geometric realizations, they all admit geometric realizations.
    Those are preserved by the truncation functors in the tower above,
    so $\mathcal{C}$ admits geometric realizations too.

    \ %

    The statement about preservation of colimits follows by a similar argument
    reducing the statement to the case of $k$-categories by virtue of
    the description of $\mathcal{C}_{\geq 0}$ as a limit of such.
  \end{proof}
\end{frame}

\begin{frame}[label=t-on-d]{The t-structure on $\mathcal{D}^-(\mathcal{A})$}
  \begin{proposition}[1.3.3.16]
    The standard t-structure on $\mathcal{D}^-(\mathcal{A})$ is
    right bounded and left complete.
  \end{proposition}

  \begin{remark}
    Left completeness is ultimately reduced to the convergence of
    Postnikov towers of spaces by embedding
    $\mathcal{D}^-(\mathcal{A})$ into the derived category of a presheaf
    category which we will be described in the next slide.
  \end{remark}
\end{frame}

\begin{frame}{A model for $\operatorname{Ind}$-objects}
  \begin{proposition}[1.3.3.13]
    $\operatorname{Ind}(\mathcal{A})$ can be identified with
    $\mathcal{A}^{\wedge} \coloneqq
    \Fun^{\times}(\mathcal{A}^\op_\text{proj}, \mathrm{Set})$
    which is again an abelian category with enough projective objects.
  \end{proposition}

  \begin{remark}[1.3.3.15]
    $\mathcal{D}^{-}(\mathcal{A})$ can be identified with the full
    subcategory of $\mathcal{D}^-(\mathcal{A}^\wedge)$ consisting
    of objects whose homology belongs to (the Yoneda image of) $\mathcal{A}$.
  \end{remark}
\end{frame}

\begin{frame}{Product preserving presheaves}
  \begin{proposition}[1.3.3.14]
    We have equivalences
    \[
      \begin{tikzcd}[ampersand replacement=\&]
        \mathcal{D}^-_{\geq 0}(\mathcal{A}^\wedge)
        \ar[
          r, leftrightarrow, swap,
          "\parbox{3em}{\center{\tiny{1.3.2.22 \\ Dold--Kan}}}"
        ] \&
        \mathcal{N}(\underline{\operatorname{Fun}}(\Delta^\op,
        \mathcal{A}^\wedge)) \ar[d, leftrightarrow]
        \& \\
        \&
        \mathcal{N}(\underline{\operatorname{Fun}}^\times(
        \mathcal{A}^\op_\text{proj}, \mathrm{sSet}))
        \ar[
          r, leftrightarrow, swap,
          "\parbox{4em}{\center{\tiny{HTT 5.5.9.3}}}"
        ] \&
        \Fun^{\times}(\mathcal{A}^\op_\text{proj}, \mathcal{S}).
      \end{tikzcd}
    \]

    Moreover, the restriction of the composite along the inclusion
    $\mathcal{A}_\text{proj} \hookrightarrow \mathcal{D}^-(\mathcal{A}^\wedge)$
    corresponds to the Yoneda embedding.
  \end{proposition}
\end{frame}

\againframe<2>{proof-diagram}

\begin{frame}[label=precise-characterization]{A more precise characterization}
  We still need to prove:
  \begin{theorem}[1.3.3.8]
    Let $\mathcal{C}$ be an $\infty$-category admitting geometric realizations
    of simplicial objects.

    Then
    \begin{enumerate}
      \item The restriction functor
        $\Fun^{|-|}(\mathcal{D}^-_{\geq 0}(\mathcal{A}),
        \mathcal{C}) \to \Fun(\mathcal{A}_\text{proj}, \mathcal{C})$ is an
        equivalence.
      \item A functor $F \in
        \Fun^{|-|}(\mathcal{D}^-_{\geq 0}(\mathcal{A}), \mathcal{C})$
        preserves finite coproducts if and only if its restriction to
        $\mathcal{A}_\text{proj}$ does.
    \end{enumerate}
  \end{theorem}
\end{frame}

\begin{frame}{One last lemma}
  Part 1 is a corollary of the following:

  \begin{lemma}[1.3.3.17]
    The essential image of $\mathcal{D}^-_{\geq 0}({\mathcal{A}})$ in
    $\Fun^{\times}(\mathcal{A}^\op_\text{proj}, \mathcal{S})$
    is the smallest full subcategory of
    $\Fun(\mathcal{A}^\op_\text{proj}, \mathcal{S})$ that contains
    the image of the Yoneda embedding and is closed under geometric
    realizations.
  \end{lemma}

  \begin{proof}[Proof sketch]
    The essential image contains the image of the Yoneda embedding.
    \hyperlink{t-on-d}{\beamergotobutton{Lemma 1.3.3.16}} and
    \hyperlink{rtex-to-rex}{\beamergotobutton{Lemma 1.3.3.11}} imply
    that it is closed under geometric realizations.

    Moreover, (the image of) every object
    $X \in \mathcal{D}^-_{\geq 0}(\mathcal{A})$ is equivalent to the geometric
    realization of the simplicial object in $\mathcal{A}_\text{proj}$
    that corresponds to (the chain complex underlying) $X$ under the
    Dold--Kan correspondence.
  \end{proof}
\end{frame}

\begin{frame}{A more precise characterization}
  \begin{theorem}[1.3.3.8]
    Let $\mathcal{C}$ be an $\infty$-category admitting geometric realizations
    of simplicial objects.

    Then
    \begin{enumerate}
      \item The restriction functor
        $\Fun^{|-|}(\mathcal{D}^-_{\geq 0}(\mathcal{A}),
        \mathcal{C}) \to \Fun(\mathcal{A}_\text{proj}, \mathcal{C})$ is an
        equivalence.
      \item A functor $F \in
        \Fun^{|-|}(\mathcal{D}^-_{\geq 0}(\mathcal{A}), \mathcal{C})$
        preserves finite coproducts if and only if its restriction to
        $\mathcal{A}_\text{proj}$ does.
    \end{enumerate}
  \end{theorem}
\end{frame}

\begin{frame}{Proof of part 2 of the more precise characterization}
  \begin{proof}[Proof sketch]
    Let $F \colon \mathcal{D}^-_{\geq 0}(\mathcal{A}) \to \mathcal{C}$ be
    a functor that preserves geometric realizations such that
    $F' \coloneqq F | \mathcal{A}_\text{proj}$ preserves finite coproducts.
    We need to show that $F$ preserves finite coproducts.

    By possibly ``extending'' $\mathcal{C}$ by virtue of HTT 5.3.5.7,
    we may assume that it has all colimits.
    Then HTT 5.5.8.15 says that $F'$, as a functor which preserves finite
    coproducts, extends to a functor in
    $\Fun^{\text{cocont}}(\Fun^\times(\mathcal{A}^\op_\text{proj},
    \mathcal{S}), \mathcal{C})$.

    Now restricting back to
    $\mathcal{D}^-_{\geq 0}(\mathcal{A}) \subseteq
    \mathcal{D}^-_{\geq 0}(\mathcal{A}^\wedge) \simeq
    \Fun^\times(\mathcal{A}^\op_\text{proj},\mathcal{S})$,
    we see that $F$ itself also preserves finite coproducts.
  \end{proof}
\end{frame}

\againframe<3>{proof-diagram}

\end{document}
