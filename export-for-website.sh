#!/bin/bash

rm -rf .assorted && mkdir -p .assorted

for DIR in *; do
  if [ -d "$DIR" ]; then
    BASE="${DIR:11}"
    cd "$DIR"
    latexmk -C
    latexmk -dvi- -pdf "${BASE}.tex"
    cp "${BASE}.pdf" ../.assorted/
    cd ..
    git archive \
      --format zip \
      --prefix "${DIR}/" \
      --output ".assorted/${DIR}.zip" \
      "HEAD:${DIR}"
  fi
done
