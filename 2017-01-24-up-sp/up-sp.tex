% Notes for a talk about the universal property of the category of spectra
% in a seminar on enhancements of triangulated categories.
% Written by Aras Ergus.

% This work is licensed under a Creative Commons Attribution-ShareAlike 4.0
% International License.
% See the following website for details:
% https://creativecommons.org/licenses/by-sa/4.0/

\documentclass[12pt,a4paper]{scrartcl}

% As of January 2017, n002-math.sty is available at:
% https://gitlab.com/aergus/n002-math
% A version compatible with this file is:
% 2016/11/23 (dda6368f729de49a7a220c0b475e0ed9e606526b)
\usepackage{n002-math}
% As of January 2017, n002-things.sty is available at:
% https://gitlab.com/aergus/n002-things
% A version compatible with this file is:
% 2016/11/23 (81b65b59488ff3b45364d3ff51dd8914062d8488)
\usepackage{n002-things}

\usepackage[
  type={CC},
  modifier={by-sa},
  version={4.0},
]{doclicense}

\bibliographystyle{alpha}

\title{Universal Properties of\\ Stable Homotopy Theory}
\author{Aras Ergus}
\date{January 24, 2017}

\curlycat

\newcommand{\der}{\mathcal{D}}
\newcommand{\spaces}{\catstyle{S}}
\newcommand{\spectra}{\mathrm{Sp}}
\newcommand{\modcatsp}{\catstyle{S\!p}}
\newcommand{\homotopyder}{\mathrm{Ho}}
\DeclareMathOperator{\Sp}{Sp}
\DeclareMathOperator{\Fun}{Fun}
\DeclareMathOperator{\Exc}{Exc}
\DeclareMathOperator{\ev}{ev}

\begin{document}
\maketitle
\doclicenseThis

\tableofcontents

\vspace{2em}

Concepts like model categories, derivators and $\infty$-categories provide
frameworks for dealing with ``homotopical theories''.
Many enhancements of triangulated categories arise in the form of such
a homotopical theory in which every object is pointed and the suspension
construction is an equivalence, \ie a ``stable'' theory.

An important example of such a theory is the stable homotopy theory, \ie
the theory of spectra up to stable equivalences.
In all of above mentioned settings, the stable homotopy theory has a certain
``universal property'' which roughly says that it is the ``free stable
homotopical theory on the sphere spectrum''.

\section{Formulation in Various Settings}\label{sect:formulations}

In this section we merely state the ``universal property'' in the above
mentioned settings without discussing the concepts that appear in detail.
A reader who is not familiar with model categories or derivators may skip
the corresponding statements as they will not play a role in the
\hyperref[sect:proof]{next section} in which we elaborate on the proof of the
statement in the setting of $\infty$-categories.

We start by formulating the statement in the settings of
model categories where a very common notion of a spectrum
is a sequence of simplicial sets $(X_n)_{n \in \NN}$ with structure
maps $(\sigma_n \colon S^1 \wedge X_n \to X_{n+1})_{n \in \NN}$.

\begin{notation}
  Let $\modcatsp$ denote the category of spectra (of simplicial sets)
  with the usual stable model structure where weak equivalences are stable
  equivalences and $\mathbb{S} \in \modcatsp$ the sphere spectrum.
\end{notation}

\begin{theorem}[\cite{SchwedeShipley02}, Theorem 5.1]
  Let $\catstyle{M}$ be a stable model category and $X$ a cofibrant and fibrant
  object of $\catstyle{M}$.
  Then
  \begin{enumerate}[label=(\roman*)]
    \item\label{quillen-existence} There is a Quillen adjunction
    \[
      X \wedge \blnk \colon \modcatsp \rightleftarrows \catstyle{M} \colon
      \Hom{X}{\blnk}
    \]
    such that $X \wedge \mathbb{S} \cong X$.
    \item Any two Quillen functor pairs as in \ref{quillen-existence} are
    related by a zigzag of natural transformations which are weak equivalences
    on cofibrant objects for the left adjoint and on fibrant objects
    for the right adjoint respectively.
  \end{enumerate}
\end{theorem}

Next, we move on to the statement in the context of derivators where
the derivator of spectra can be obtained from the model category of
spectra by passing to homotopy derivators.
Here the statement is somewhat more concise, but requires studying
morphisms of derivators.

\begin{definition}
  Let $\der, \der' \colon \mathcal{F\!i\!n\!C\!a\!t}^\mathrm{op} \to \CAT$ be
  prederivators.
  A \emph{morphism of prederivators} $\der \to \der'$ is given by a
  ``pseodonatural transformation'', \ie a collection
  $(F_\bullet, \gamma_\bullet)$ consisting of
  \begin{itemize}
    \item a functor $F_I \colon \der(I) \to \der'(I)$ for every finite category
    $I$ and
    \item a natural isomorphism $\gamma_u \colon u^*_{\der'} F_J
    \xRightarrow{\cong} F_I u^*_\der$ for every functor $u \colon I \to J$
    between finite categories
  \end{itemize}
  such that
  \begin{itemize}
    \item $\gamma_{\id_I} = \id_{F_I}$ for all $I \in
    \mathcal{F\!i\!n\!C\!a\!t}$,
    \item $\gamma_{vu} = \gamma_u \gamma_v$ for $I \xrightarrow{u} J
    \xrightarrow{v} K$ in $\mathcal{F\!i\!n\!C\!a\!t}$,
    \item $\alpha^*_\der \gamma_{u_1} = \gamma_{u_2} \alpha^*_{\der'}$ for
    a natural transformation $\alpha \colon u_1 \Rightarrow u_2$ in
    $\mathcal{F\!i\!n\!C\!a\!t}$.
  \end{itemize}
\end{definition}

\begin{definition}
  Let $(F, \gamma), (G, \delta) \colon \der \to \der'$ be morphisms
  of prederivators.
  A \emph{natural transformation} $(F, \gamma) \Rightarrow (G, \delta)$ is
  given by a ``modification'', \ie a collection of natural transformations
  $(\tau_I \colon F_I \Rightarrow G_I)_{I \in \mathcal{F\!i\!n\!C\!a\!t}}$
  such that for all $u \colon I \to J$ in $\mathcal{F\!i\!n\!C\!a\!t}$ we have
  $\delta_u \tau_J = \tau_I \gamma_u$.

  Morphisms of prederivators with natural transformations as compositions
  and evident compositions \resp identities yields a category
  $\mathcal{M\!o\!r}(\der, \der')$ of morphisms between two prederivators.
\end{definition}

\begin{definition}
  Let $\der$, $\der'$ be derivators.
  A morphism $(F, \gamma) \colon \der \to \der'$ of prederivators is called
  \emph{exact} if for all $u \colon I \to J$ in $\mathcal{F\!i\!n\!C\!a\!t}$,
  the induced natural transformations $u_! F \Rightarrow F u_!$ and
  $F u_* \Rightarrow u_* F$ are isomorphisms.

  By $\mathcal{M\!o\!r}^\mathrm{ex}(\der, \der')$ we denote the full subcategory
  of $\mathcal{M\!o\!r}(\der, \der')$ spanned by exact morphisms.
\end{definition}

\begin{notation}
  Let $\homotopyder_{\modcatsp^\mathrm{fin}}$ be the subderivator of
  the homotopy derivator of $\modcatsp$ given by diagrams of spectra
  with finitely many cells.
\end{notation}

\begin{theorem}[\cite{Franke96}, Theorem 4]
  Let $\der$ be a stable derivator.
  Then evaluation at the sphere spectrum induces an equivalence of categories
  \[
    \mathcal{M\!o\!r}^{\mathrm{ex}}(\homotopyder_{\modcatsp^\mathrm{fin}}, \der)
    \xrightarrow{\simeq} \der(\Asterisk).
  \]
\end{theorem}

In \cite{Lurie16}, the $\infty$-category $\spectra$ of spectra and the sphere
spectrum is defined in a different way than the usual approach with sequences
of simplicial sets and structure maps.
We now state the theorem as-is and are going to deal with it in detail in the
\hyperref[sect:proof]{next section}.

\begin{theorem}[\cite{Lurie16}, Corollary 1.4.4.6]\label{state:univ-prop-infty}
  Let $\catstyle{C}$ be a presentable stable $\infty$-category.
  Then evaluation at the sphere spectrum induces an equivalence
  \[
    \Fun^\mathrm{L}(\spectra, \catstyle{C}) \simeq \catstyle{C}
  \]
  between the $\infty$-category of functors from $\spectra$ to $\catstyle{C}$
  which admit a right adjoint and $\catstyle{C}$.
\end{theorem}

\section{Proof Sketch in the Setting of $\infty$-categories}\label{sect:proof}

We start by recalling \resp stating some basics about (stable) $\infty$
categories.

\begin{definition}
  A pointed $\infty$-category $\catstyle{C}$ is \emph{stable} if one
  of the following equivalent conditions holds:
  \begin{itemize}
    \item Every morphism in $\catstyle{C}$ admits a fiber and a cofiber,
    fiber sequences and cofiber sequences coincide.
    \item $\catstyle{C}$ has finite limits and colimits, pullback squares
    and pushout squares coincide.
    \item$\catstyle{C}$ has finite limits and colimits, the loop functor
    $\Omega \colon \catstyle{C} \to \catstyle{C}$ is an equivalence.
  \end{itemize}
\end{definition}

Next, we fix some notation.

\begin{notation}
  Let
  \begin{itemize}
    \item $\spaces$ denote the $\infty$-category of spaces (as modeled by
    simplicial sets),
    \item $\asterisk \in \spaces$ a terminal object,
    \item $\spaces^\mathrm{fin}$ the smallest full subcategory of $\spaces$
    which contains $\asterisk$ and is closed under finite colimits,
    \item $\spaces^\mathrm{fin}_* \subseteq \spaces_*$ the $\infty$-category
    of pointed objects of $\spaces^\mathrm{fin}$.
  \end{itemize}
\end{notation}

In fact, already $\spaces^\mathrm{fin}$ and $\spaces$ have universal properties
which will be very useful.
However, the proofs these properties, which can be extracted from \cite{Lurie09}
(\cf Remark 5.3.5.9, Proposition 4.3.2.15 and Theorem 5.1.5.6 there),
could probably fill another talk.

\begin{definition}
  Let $F \colon \catstyle{C} \to \catstyle{D}$ between $\infty$-categories
  with finite colimits.
  $F$ is called \emph{right exact} if it preserves finite colimits.
  Let $\Fun^\mathrm{Rex}(\catstyle{C}, \catstyle{D}) \subseteq
  \Fun(\catstyle{C}, \catstyle{D})$ be the full subcategory spanned by
  right exact functors.

  There is, of course, a dual notion of \emph{left exact} functors.
\end{definition}

\begin{fact}\label{state:univ-prop-of-finite-spaces}
  Let $\catstyle{C}$ be an $\infty$-category with finite colimits.
  Then evaluation at $\asterisk \in \spaces^\mathrm{fin}$ induces an equivalence
  \[
    \ev_\asterisk \colon \Fun^\mathrm{Rex}(\spaces^\mathrm{fin}, \catstyle{C})
    \xrightarrow{\simeq} \catstyle{C}.
  \]
\end{fact}

For the ``universal property'' of $\spaces$ we will need the concept of an
adjoint functor.

\begin{definition}
  Let $f \colon \catstyle{C} \to \catstyle{D}$ and $g \colon \catstyle{D} \to
  \catstyle{C}$ be a pair of functors between $\infty$-categories.
  Then we call $f$ \emph{left adjoint to $g$} (\resp $g$ \emph{right
  adjoint to $f$}) if there exits a morphism
  $u \colon \id_\catstyle{C} \to g \circ f$  (``unit transformation'') in
  $\Fun(\catstyle{C}, \catstyle{C})$ such that for all $C \in \catstyle{C}$,
  $D \in \catstyle{D}$ the composite
  \[
    \operatorname{Map}_\catstyle{D}(f(C), D) \xrightarrow{g}
    \operatorname{Map}_\catstyle{C}(g(f(C)), g(D)) \xrightarrow{u(C)}
    \operatorname{Map}_\catstyle{C}(C, g(D))
  \]
  is a weak equivalence.

  There is also a description of adjunctions using ``counit transformations''.
\end{definition}

\begin{fact}
  Left adjoint functors between $\infty$-categories preserve all small
  colimits and right adjoint functors between $\infty$-categories preserve all
  small limits.
\end{fact}

\begin{notation}
  Let $\catstyle{C}$, $\catstyle{D}$ be $\infty$-categories.
  Set $\Fun^\mathrm{R}(\catstyle{C}, \catstyle{D}) \subseteq
  \Fun(\catstyle{C}, \catstyle{D})$ to be the full subcategory spanned by
  functors which admit a left adjoint and
  $\Fun^\mathrm{L}(\catstyle{C}, \catstyle{D}) \subseteq
  \Fun(\catstyle{C}, \catstyle{D})$ the full subcategory spanned by
  functors which admit a right adjoint.
\end{notation}

\begin{fact}
  Adjoint functors are ``unique up to homotopy'' in the sense that
  there is a canonical equivalence
  \[
    \Fun^\mathrm{L}(\catstyle{C}, \catstyle{D})^\mathrm{op} \simeq
    \Fun^\mathrm{R}(\catstyle{D}, \catstyle{C}).
  \]
\end{fact}

\begin{fact}\label{state:univ-prop-of-spaces}
  Let $\catstyle{C}$ be an $\infty$-category with small colimits.
  Then evaluation at $\asterisk \in \spaces$ induces an equivalence
  \[
  \ev_\asterisk \colon \Fun^\mathrm{L}(\spaces, \catstyle{C})
  \xrightarrow{\simeq} \catstyle{C}.
  \]
\end{fact}

We are now going to define an $\infty$-category of spectra.
The rough idea is defining a spectrum via its ``homology theory'', which
is axiomatized by the concept of an \emph{excisive functor}:

\begin{definition}
  Let $F \colon \catstyle{C} \to \catstyle{D}$ be a functor between
  $\infty$-categories.
  \begin{itemize}
    \item Assuming $\catstyle{C}$ has pushouts, $F$ is called \emph{excisive}
    if $F$ carries pushout squares in $\catstyle{C}$ to pullback squares
    in $\catstyle{D}$.
    We denote by $\Exc(\catstyle{C}, \catstyle{D})$ the full subcategory
    of $\Fun(\catstyle{C}, \catstyle{D})$ spanned by excisive functors.
    \item Assuming $\catstyle{C}$ has a terminal object $\asterisk$, $F$ is
    called \emph{reduced} if $F(\asterisk)$ is a terminal object of
    $\catstyle{D}$.
    We denote by $\Fun_*(\catstyle{C}, \catstyle{D})$ the full subcategory
    of $\Fun(\catstyle{C}, \catstyle{D})$ spanned by reduced functors.
    \item If $\catstyle{C}$ admits pushouts and a terminal object, we define
    \[
      \Exc_*(\catstyle{C}, \catstyle{D}) \coloneqq
      \Exc(\catstyle{C}, \catstyle{D}) \cap
      \Fun_*(\catstyle{C}, \catstyle{D}).
    \]
  \end{itemize}
\end{definition}

\begin{definition}
  Let $\catstyle{C}$ be an $\infty$-category with finite limits.
  The \emph{$\infty$-category of spectrum objects of $\catstyle{C}$} is
  defined as $\Sp(\mathcal{C}) \coloneqq
  \Exc_*(\spaces^\mathrm{fin}_*, \catstyle{C})$.

  Let $\spectra \coloneqq \Sp(\spaces)$ denote the category of spectra.
\end{definition}

An important property of the $\infty$-category of spectrum objects is the
fact that it is stable.

\begin{proposition}\label{state:exc-is-stable}
  Let $\catstyle{C}$ be a pointed $\infty$-category with finite colimits and
  $\catstyle{D}$ an $\infty$-category with finite limits.
  Then $\Exc_*(\catstyle{C}, \catstyle{D})$ is stable.
\end{proposition}
\begin{proof}[Proof sketch]
  % MAYDO: more details
  \begin{itemize}
    \item The constant functor on a terminal object of $\catstyle{D}$ is
    a zero object of $\Exc_*(\catstyle{C}, \catstyle{D})$:
    It is terminal since it is terminal in $\Fun(\catstyle{C}, \catstyle{D})$.
    It is initial in $\Fun_*(\catstyle{C}, \catstyle{D})$ since
    it is the Kan extension of its restriction along the inclusion
    $\setbrack{\asterisk} \to \catstyle{C}$ of a terminal object
    and thus for $Y \in \Fun_*(\catstyle{C}, \catstyle{D})$ we have
    $\operatorname{Map}_{\Fun(\catstyle{C}, \catstyle{D})}(X, Y) \simeq
    \operatorname{Map}_{\catstyle{D}}(X(\asterisk), Y(\asterisk)) \simeq
    \asterisk$.
    \item Finite limits are inherited from $\Fun(\catstyle{C}, \catstyle{D})$
    since these can be computed pointwise and thus commute with pullbacks
    in $\catstyle{D}$.
    \item Precomposition with the suspension functor of $\catstyle{C}$ is
    a homotopy inverse to $\Omega_{\Exc_*(\catstyle{C}, \catstyle{D})}$:
    For $X \in \Exc_*(\catstyle{C}, \catstyle{D})$, $C \in \catstyle{C}$ we have
    \[
      \begin{tikzcd}
        C \ar[r] \ar[d] \ar[phantom]{rd}{\text{po}} & * \ar[d] \\
        * \ar[r] & \Sigma_\catstyle{C} C
      \end{tikzcd}
      \leadsto
      \begin{tikzcd}
        X(C) \ar[r] \ar[d] \ar[phantom]{rd}{\text{pb}}  & * \ar[d] \\
        * \ar[r] & X(\Sigma_\catstyle{C} C)
      \end{tikzcd},
    \]
    thus $\Omega_{\catstyle{D}}(X(\Sigma_\catstyle{C} C)) \simeq X(C)$.
    Hence $(\Omega_{\Exc_*(\catstyle{C}, \catstyle{D})}X) \circ
    \Sigma_\catstyle{C} \simeq X$.
    \item In order to obtain finite colimits (even if $\catstyle{D}$ does not
    admits colimits), one has to consider the Yoneda
    embedding of $\catstyle{D}$ and use that it is closed under limits in
    its presheaf category.
  \end{itemize}
\end{proof}

\begin{corollary}\label{state:sp-is-stable}
  Let $\catstyle{C}$ be an $\infty$-category with finite limits.
  Then $\Sp(\catstyle{C})$ is stable.
\end{corollary}

Next, we are going to establish a criterion for showing that an
$\infty$-category is stable.

\begin{lemma}\label{state:base-point-fix}
  Let $\catstyle{C}$ be an $\infty$ category which admits finite colimits
  and a terminal object $\asterisk$, $f \colon \catstyle{C} \to \catstyle{C}_*$
  a left adjoint to the forgetful functor and $\catstyle{D}$ a stable
  $\infty$-category.
  Let $\Exc'(\catstyle{C}, \catstyle{D}) \subseteq
  \Exc(\catstyle{C}, \catstyle{D})$  be spanned by the functors which
  carry the initial object of $\catstyle{C}$ to the terminal object
  of $\catstyle{D}$.
  Then precomposition with $f$ induces an equivalence of $\infty$-categories
  \begin{equation}\label{base-point-fix-functor}
    \Exc_*(\catstyle{C}_*, \catstyle{D}) \xrightarrow{\simeq}
    \Exc'(\catstyle{C}, \catstyle{D})
  \end{equation}
\end{lemma}
\begin{proof}
  Let $X \in \Exc_*(\catstyle{C}_*, \catstyle{D})$, $\emptyset \in \catstyle{C}$
  initial.
  Then $X(f(\emptyset)) \simeq X(\asterisk)$ is terminal in $\catstyle{D}$ since
  $X$ is reduced.
  Moreover, since $f$ is a left adjoint, it preserves pushout squares.
  Hence $X \circ f$ maps pushout squares to pullback squares since $X$ does.
  Thus we get a well-defined functor as in (\ref{base-point-fix-functor}).

  Now consider the composition
  \[
    \Fun(\catstyle{C}, \catstyle{D}) \times \catstyle{C}_*
    \subseteq \Fun(\catstyle{C}, \catstyle{D}) \times
    \Fun(\Delta^1, \catstyle{C}) \xrightarrow{\circ}
    \Fun(\Delta^1, \catstyle{D}) \xrightarrow{\operatorname{cofib}}
    \catstyle{D},
  \]
  which corresponds to a functor $\theta \colon
  \Fun(\catstyle{C}, \catstyle{D}) \to \Fun(\catstyle{C}_*, \catstyle{D})$.
  For $Y \in \Exc'(\catstyle{C}, \catstyle{D})$ we have that
  $\theta(Y)(\asterisk \xrightarrow{\id} \asterisk) \simeq
  \operatorname{cofib}(Y(\asterisk) \xrightarrow{\id} Y(\asterisk))$
  is terminal in $\catstyle{D}$, so $\theta(Y)$ is reduced.
  Since pushout diagrams in $\catstyle{C}_*$ are detected by the forgetful
  functor $\catstyle{C}_* \to \catstyle{C}$, the image of a ``pushout cube''
  in $\catstyle{C}_* \subseteq \Fun(\Delta^1, \catstyle{C})$ under $Y$
  has pullback (hence, by the stability of $\catstyle{D}$, pushout) squares
  in appropriate faces.
  % TODO: draw a diagram, maybe?
  Taking cofibers produces another pushout (and hence pullback) square,
  which shows that $\theta(Y)$ is also excisive.
  Hence $\theta$ restricts to a functor $\Exc'(\catstyle{C}, \catstyle{D}) \to
  \Exc_*(\catstyle{C}_*, \catstyle{D})$.

  Now we have natural equivalences
  $\theta(X \circ f)(C') \simeq \operatorname{cofib}(X(f(\asterisk))
  \to X(f(C'))) \simeq X(C')$ for
  a pointed object $C' \in \catstyle{C}_*$ and
  % TODO: Why?
  $(\theta(Y) \circ f)(C) \simeq \operatorname{cofib}(Y(\asterisk) \to
  Y(f(C))) \simeq Y(C)$ for $X \in \catstyle{C}$,
  so $\theta$ is a homotopy inverse to $\blnk \circ f$.
\end{proof}

\begin{definition}
  Let $\catstyle{C}$ be an $\infty$-category with finite limits.
  Fix a left adjoint $f \colon \spaces^\mathrm{fin} \to \spaces^\mathrm{fin}_*$
  to the forgetful functor (which is essentially given by adjoining a base
  point).
  Let $S^0 \coloneqq f(\asterisk) \in \spaces^\mathrm{fin}_*$ denote the
  $0$-sphere.
  Then evaluation at $S^0$ defines a functor
  $\Omega^\infty \coloneqq \ev_{S^0} \colon \Sp(\catstyle{C}) \to
  \catstyle{C}$.
\end{definition}

\begin{proposition}\label{state:stability-criterion}
  Let $\catstyle{C}$ be an $\infty$ category with finite limits.
  Then the following are equivalent:
  \begin{enumerate}[label=(\roman*)]
    \item\label{stability-generic} $\catstyle{C}$ is stable.
    \item\label{stability-by-omega} $\Omega^\infty \colon \Sp(\catstyle{C}) \to
    \catstyle{C}$ is an equivalence.
  \end{enumerate}
\end{proposition}
\begin{proof}
  (\ref{stability-generic} $\Rightarrow$ \ref{stability-by-omega}):
  $\Omega^\infty$ factors as the composition
  \[
    \Exc_*(\spaces^\mathrm{fin}_*, \catstyle{D}) \xrightarrow{\blnk \circ f}
    \Exc'(\spaces^\mathrm{fin}, \catstyle{D}) \xrightarrow{\ev_\asterisk}
    \catstyle{D},
  \]
  where $f \colon \spaces^\mathrm{fin} \to \spaces^\mathrm{fin}_*$ is given
  by ``adding a base point''.
  The first arrow is an equivalence by \autoref{state:base-point-fix}, so it
  is enough to show that $\ev_\asterisk \colon
  \Exc'(\spaces^\mathrm{fin}, \catstyle{C}) \to \catstyle{C}$ is an equivalence.

  Now in $\catstyle{C}$, terminal objects coincide with initial objects
  and pullback squares coincide with pushout squares since $\catstyle{C}$
  is stable.
  Thus, $\Exc'(\spaces^\mathrm{fin}, \catstyle{C})$ coincides with
  $\Fun^\mathrm{Rex}(\spaces^\mathrm{fin}, \catstyle{C})$ and
  $\ev_\asterisk \colon \Exc'(\spaces^\mathrm{fin}, \catstyle{C}) \to
  \catstyle{C}$ is indeed an equivalence by
  \autoref{state:univ-prop-of-finite-spaces}.

  (\ref{stability-by-omega} $\Rightarrow$ \ref{stability-generic}):
  Follows from the fact that $\Sp(\catstyle{C})$ is stable
  (by \autoref{state:sp-is-stable}).
\end{proof}

We will use this criterion while working towards the proof of our main result.

\begin{proposition}\label{state:omega-infty-equiv}
  Let $\catstyle{C}$ be a pointed $\infty$-category with finite colimits and let
  $\catstyle{D}$ an $\infty$-category with finite limits.
  Then postcomposition with $\Omega^\infty_\catstyle{D} \colon
  \Sp(\catstyle{D}) \to \catstyle{D}$ induces an equivalence
  of $\infty$-categories
  \[
    (\Omega^\infty_\catstyle{D} \circ \blnk) \colon
    \Exc_*(\catstyle{C}, \Sp(\catstyle{D}))
    \xrightarrow{\simeq} \Exc_*(\catstyle{C}, \catstyle{D}).
  \]
\end{proposition}
\begin{proof}
  The usual exponential isomorphism restricts to an isomorphism
  $\Exc_*(\catstyle{C}, \Sp(\catstyle{D})) \cong
  \Sp(\Exc_*(\catstyle{C}, {D}))$, under which
  postcomposition with $\Omega^\infty_\catstyle{D}$ corresponds to
  \[
  \Omega^\infty_{\Exc_*(\catstyle{C}, \catstyle{D})} \colon
  \Sp(\Exc_*(\catstyle{C}, \catstyle{D})) \to
  \Exc_*(\catstyle{C}, \catstyle{D}),
  \]
  which is, by \autoref{state:stability-criterion},
  an equivalence since $\Exc_*(\catstyle{C}, \catstyle{D})$
  is stable by \autoref{state:exc-is-stable}.
\end{proof}

We will now have an interlude on presentable $\infty$-categories
in which we are only going to give a few definitions and state some facts
which are required to understand our main result.
Details on this subject can be found in \cite{Lurie09}, Chapter 5.

\begin{definition}
  An $\infty$-category $\catstyle{C}$ is called \emph{accessible} if there
  exists a regular cardinal $\kappa$ such that
  \begin{itemize}
    \item $\catstyle{C}$ has small $\kappa$-filtered colimits,
    \item $\catstyle{C}$ contains an essentially small subcategory
    $\catstyle{C}'$ consisting of $\kappa$-compact objects which
    generate $\catstyle{C}$ under $\kappa$-filtered colimits.
  \end{itemize}
\end{definition}

\begin{definition}
  An $\infty$-category is called \emph{presentable} if it is accessible
  and admits small colimits.
\end{definition}

\begin{example}
  The $\infty$-category $\spaces$ of spaces is presentable.
\end{example}

In general, simplicial combinatorial model categories give rise to presentable
$\infty$-categories (\cf \cite{Lurie09}, Proposition A.3.7.6).

The crucial statement about presentable $\infty$-categories is the following
``adjoint functor theorem'':

\begin{fact}\label{state:aft}
  Let $F \colon \catstyle{C} \to \catstyle{D}$ be a functor between presentable
  $\infty$-categories. Then:
  \begin{itemize}
    \item $F$ has a right adjoint if and only if it preserves small colimits.
    \item $F$ has a left adjoint if and only if it preserves $\kappa$-filtered
    colimits for some regular cardinal $\kappa$ and preserves small limits.
  \end{itemize}
\end{fact}

The proof of the following fact uses the adjoint functor theorem.

\begin{fact}\label{state:stability-and-presentability}
  Let $\catstyle{C}$, $\catstyle{D}$ be presentable $\infty$-categories and let
  $\catstyle{D}$ be stable.
  Then
  \begin{enumerate}[label=(\roman*)]
    \item $\Sp(\catstyle{C})$ is presentable.
    \item $\Omega^\infty \colon \Sp(\catstyle{C}) \to \catstyle{C}$ admits
    a left adjoint $\Sigma^\infty_+ \colon \catstyle{C} \to \Sp(\catstyle{C})$.
    % MAYDO: use the definition of an exact functor instead of rexcisive
    \item\label{exactness-omega-left-adj} A reduced excisive functor
    $G \colon \catstyle{D} \to \Sp(\catstyle{C})$ admits a left adjoint if
    and only if $\Omega^\infty \circ G \colon \catstyle{D}
    \to \catstyle{C}$ admits a left adjoint.
  \end{enumerate}
\end{fact}
\begin{proof}[Proof idea]
  Describe $\Sp(\catstyle{C})$ as the homotopy limit of the tower
  ($\ldots \xrightarrow{\Omega} \catstyle{C}_* \xrightarrow{\Omega}
  \catstyle{C}_*$).
\end{proof}

Now we have everything at hand to prove the main result.

\begin{corollary}\label{state:sigma-infty-equiv}
  Let $\catstyle{D}$, $\catstyle{C}$ be presentable $\infty$-categories and
  suppose that $\catstyle{C}$ is stable.
  % TODO: define the suspension functor -- see proof of 1.4.4.4
  Then precomposition with $\Sigma^\infty_+$ induces an equivalence
  \begin{equation}\label{sigma-infty-morph}
    (\blnk \circ \Sigma^\infty_+) \colon
    \Fun^\mathrm{L}(\Sp(\catstyle{D}), \catstyle{C})
    \xrightarrow{\simeq}
    \Fun^\mathrm{L}(\catstyle{D}, \catstyle{C}).
  \end{equation}
\end{corollary}
\begin{proof}
  By ``passing to right adjoints'', we see that (\ref{sigma-infty-morph}) is an
  equivalence if and only if
  % TODO: Why are these equivalent?
  \[
    (\Omega^\infty \circ \blnk) \colon
    \Fun^\mathrm{R}(\catstyle{C}, \Sp(\catstyle{D})) \to
    \Fun^\mathrm{R}(\catstyle{C}, \catstyle{D})
  \]
  is an equivalence.
  Now by stability of $\catstyle{C}$, every functor from $\catstyle{C}$ which
  admits a left adjoint (and hence commutes with limits) is reduced and
  excisive.
  Hence, since by
  \autoref{state:stability-and-presentability}\ref{exactness-omega-left-adj}
  postcomposition with $\Omega^\infty$ ``detects'' reduced excisive functors
  which admit a left adjoint, the equivalence $(\Omega^\infty \circ \blnk)
  \colon \Exc_*(\catstyle{C}, \Sp(\catstyle{D})) \to
  \Exc_*(\catstyle{C}, \catstyle{D})$ of \autoref{state:omega-infty-equiv}
  restricts to functors which admit a left adjoint.
\end{proof}

\begin{notation}
  Let $S \coloneq \Sigma^\infty_+(\asterisk) \in \Sp$ denote the sphere
  spectrum.
\end{notation}

\begin{corollary}[=\autoref{state:univ-prop-infty}]
  Let $\catstyle{C}$ be a presentable stable $\infty$-category. Then
  the evaluation at the sphere spectrum induces an equivalence
  \[
    \ev_S \colon \Fun^\mathrm{L}(\spectra, \catstyle{C}) \xrightarrow{\simeq}
    \catstyle{C}.
  \]
\end{corollary}
\begin{proof}
  Since $\spectra = \Sp(\spaces)$, $\ev_S = \ev_{\Sigma^\infty_+(\asterisk)}$
  factors as
  \[
    \Fun^\mathrm{L}(\spectra, \catstyle{C})
    \xrightarrow{\blnk \circ \Sigma^\infty_+}
    \Fun^\mathrm{L}(\spaces, \catstyle{C}) \xrightarrow{\ev_\asterisk}
    \catstyle{C}.
  \]
  Now the first arrow is an equivalence by \autoref{state:sigma-infty-equiv} and
  the second was is an equivalence by \autoref{state:univ-prop-of-spaces}.
\end{proof}

\bibliography{up-sp}{}

\end{document}
